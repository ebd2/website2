<?php
use Symfony\Component\Debug\Debug;


require_once __DIR__.'/vendor/autoload.php';

$app = require __DIR__.'/src/app.php';

// If on localhost use the dev page.. OTherwise use the production page

//var_dump ($_SERVER["HTTP_HOST"]);

if (preg_match('/10\./',$_SERVER['REMOTE_ADDR']) || in_array(@$_SERVER['REMOTE_ADDR'], array('git.','dev.','10.0.2.2', '127.0.0.1', 'fe80::1', '::1')) || $_SERVER["HTTP_HOST"]=="git.somervilleopenstudios.org") {
    ini_set('display_errors', 1);
    Debug::enable();
    require __DIR__.'/config/dev.php';

} else {
    ini_set('display_errors', 0);
    require __DIR__.'/config/prod.php';


}

require __DIR__.'/src/controllers.php';
$app->run();
