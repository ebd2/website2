<?php

use Symfony\Component\Debug\Debug;

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.


/*
var_dump ($_SERVER['REMOTE_ADDR']);

var_dump ($_SERVER['HTTP_CLIENT_IP'] );
var_dump ($_SERVER['HTTP_X_FORWARDED_FOR']);
*/

/*if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('10.0.', '127.0.0.1', 'fe80::1', '::1'))
if (!preg_match('/10\./',$_SERVER['REMOTE_ADDR']) || !in_array(@$_SERVER['REMOTE_ADDR'], array('10.0.2.2', '127.0.0.1', 'fe80::1', '::1') )
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}
*/
require_once __DIR__.'/vendor/autoload.php';

Debug::enable();

$app = require __DIR__.'/src/app.php';
require __DIR__.'/config/dev.php';
require __DIR__.'/src/controllers.php';
$app->run();
