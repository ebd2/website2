<?php

// configure your app for the production environment

$app['twig.path'] = array(__DIR__.'/../templates');
//$app['twig.form.templates'] = array("bootstrap_3_horizontal_layout.html.twig");
$app['twig.form.templates'] = array("bootstrap_3_layout.html.twig");

//$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');


// SOS PDO DATABASE object



$s= $_SERVER['HTTP_HOST'];
if (strpos($s,'dev.somer') != false){

    $app['pdo.dsn'] = 'mysql:dbname=database_name';
    $app['pdo.user'] = 'my_username';
    $app['pdo.password'] = 'my_password';
} else {
    $app['pdo.dsn'] = 'mysql:dbname=database_name';
    $app['pdo.user'] = 'my_username';
    $app['pdo.password'] = 'my_password';
}


$app['pdo'] = new PDO(
    $app['pdo.dsn'],
    $app['pdo.user'],
    $app['pdo.password']);


