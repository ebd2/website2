<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../vendor/autoload.php';
require_once 'dboClass.php';
require_once 'do_dump.php';
// Create a payment.

$dbo=  dboClass::getDBO();

print "<h3>+++ Tests for the ArtistsGenreList class </h3>";

$artistObj = new \SOSModels\ArtistsGenreList($dbo);



$geneFromForm = array ( 'Books', 'Other_Media', 'Photography' );
$geneFromForm = array ( 'Glass', 'Graphic', 'Installation' );
$otherString ='';

print "<b>+++ createGenreYNArray ()</b><br> ";

$ynGenreList = $artistObj->createGenreYNArray($geneFromForm, $otherString, $dbo);
var_dump ($ynGenreList);

print "<b>+++ createGenreYNArray - no changes </b><br> ";


// generate a fake genre list as would come from an artist's existing profiles.
$originalData = \SOSModels\ArtistsGenreList::$column_name_to_genre_name;


// setting to X insures the test will run for all fields (Since in the database its all Y or N
foreach ($originalData as $key=>$value){

    $originalData[$key] ="X";

}
$originalData['Other']='Other Text';
$ynGenreList['Other']='Neww Other Text';

$results = $artistObj->updateGenreData ($ynGenreList, $originalData, 592,$dbo);

var_dump ($results);

echo "done...";


