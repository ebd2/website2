<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../vendor/autoload.php';
require_once 'dboClass.php';
require_once 'do_dump.php';
// Create a payment.

$pdo=  dboClass::getDBO();



$artistObj = new \SOSModels\ArtistData($pdo, 37);

print "+++ ArtistData getArtistInfo()<br>";

$artistData = $artistObj->getArtistInfo();
do_dump ($artistData);



print "+++ getUserFeatures() <br>";
$userFeature = $artistObj->getUserFeatures();
do_dump ($userFeature);


print "<h3> Map Data ------------------</h3>";
// get the json of one artists data
if (empty($artistData['map_number'])){
    $artistData['map_number'] = 0;
}
$artistsListObj = new \SOSModels\ArtistsList($pdo);
$mapJson = $artistsListObj->listToJson(array($artistData), "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');
print "$mapJson";




print "<h3> Images ------------------</h3>";

print "+++ getArtistImagesData() <br>";
$artistImageData = $artistObj->getArtistImagesData();
do_dump ($artistImageData);


$artistThumbnailData = $artistObj->getArtistThumbnailData();
print "+++ getArtistThumbnailData() <br>";
do_dump ($artistThumbnailData);



echo "done...";


