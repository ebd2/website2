<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../vendor/autoload.php';
require_once 'dboClass.php';
require_once 'do_dump.php';
// Create a payment.

$pdo=  dboClass::getDBO();

print "<h3>+++ Tests for the SQLhelper class </h3>";

// for testing we'll use the Genre Array..


$arrayKeysToCheck =  array('Books', 'Collage', 'Drawing', 'Fiber', 'Furniture', 'Glass');
$originalData =   array('Books' => 'N',
    'Collage' => 'N',
    'Drawing' => 'N',
    'Fiber' => 'N',
    'Furniture'=>'N',
    'Glass'=>'N');

$newData = array('Books' => 'N',
    'Collage' => 'N',
    'Drawing' => 'N',
    'Fiber' => 'N',
    'Furniture'=>'N',
    'Glass'=>'N');

print "<b>+++ findChangesInArray - no changes </b><br> ";
var_dump ($arrayKeysToCheck, $originalData, $newData);
$changes = \SOS\SQLHelper::findChangesInArray($arrayKeysToCheck, $originalData, $newData);
print "<b> Results:</b> <br>";
var_dump ($changes);


$newData = array('Books' => 'Y',
    'Collage' => 'N',
    'Drawing' => 'N',
    'Fiber' => 'N',
    'Furniture'=>'N',
    'Glass'=>'NY');

print "<b>+++ findChangesInArray - some changes </b><br> ";
var_dump ($arrayKeysToCheck, $originalData, $newData);
$changes = \SOS\SQLHelper::findChangesInArray($arrayKeysToCheck, $originalData, $newData);
print "<b> Results:</b> <br>";
var_dump ($changes);




echo "done...";


