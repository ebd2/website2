<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 11/5/18
 * Time: 8:39 PM
 */

class dboClass
{


    public static function getDBO ()
    {
        $app['pdo.dsn'] = 'mysql:dbname=somerville_dev';
        $app['pdo.user'] = 'root';
        $app['pdo.password'] = 'root';


        $pdo = new PDO(
            $app['pdo.dsn'],
            $app['pdo.user'],
            $app['pdo.password']);

        return $pdo;

    }

}