<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

#use SOSControllers;
#use SOSControllers\VolunteerController;
//$app->register(new Silex\Provider\SessionServiceProvider());

// HOMEPAGE

$userInfo = $app['session']->get('sos_user_info');

if (!is_null($userInfo)){
    //var_dump ($userInfo);
    $app['username']= $userInfo ['EmailAddress'] ;
}



$app->match('/',  "SOSControllers\\HomePage::index")->bind('homepage');


$app->match('/example',function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('example_sidebar.html.twig');
})->bind('example');


$app->match('/aram', function () use ($app) {

    return "Arams";
});
//--------------------
// API / Ajax Data


$app->match('api/get_street_names/{string}',  "SOSControllers\\AjaxData::completeStreetLocation")->bind('api_get_street_names');

//----------------------------
// Event Pages
//----------------------------

$app->match('/event/{event_name}',  "SOSControllers\\EventsController::showEventPage")->bind('show_event_page');



//----------------------------
// Visitor Pages
//----------------------------

$visitMenu = \SOSModels\Menu::$visitMenu;

$visit = $app['controllers_factory'];

$visit->get('/', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;

    return $app['twig']->render('visit/index.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.home');

$visit->get('/artists_list', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;

    return $app['twig']->render('visit/artist_list.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.artist_listing');

$visit->get('/events', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;

    return $app['twig']->render('visit/visitor_events.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.events');

$visit->get('/FAQ', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;
    return $app['twig']->render('visit/faq.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.faq');

$visit->get('/visitor_calendar', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;
    return $app['twig']->render('visit/calendar.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.calendar');

$app->mount('/visit', $visit);



//---------------------------
// Artist Profile/ Artist Directory
//---------------------------
$app['artistsMenu'] = array("ARTISTS DIRECTORY"=>'artists_directory');

$app->match('artists/artist_profile/{id}', "SOSControllers\\ArtistProfileController::showProfile")->bind('artists_profile');

$app->match('artists/artist_directory', "SOSControllers\\ArtistsDirectoryHomeController::generatePage")->bind('artists_directory');

//random
$app->match('artists/artist_profile_random', "SOSControllers\\ArtistsDirectoryController::randomProfile")->bind('artists_profile_random');

$app->match('artists/artist_directory/random/{type}', "SOSControllers\\ArtistsDirectoryController::showRandom")->bind('artists_directory_random');

$app->match('artists/artist_directory/genre/{genre}', "SOSControllers\\ArtistsDirectoryController::showListbyGenre")->bind('artists_directory_list_genre');

$app->match('artists/artist_directory/search/{search}', "SOSControllers\\ArtistsDirectoryController::showListbySearchTerm")->bind('artists_directory_search');

$app->match('artists/artist_directory/genre/{type}/{genre}', "SOSControllers\\ArtistsDirectoryController::showListbyGenre2")->bind('artists_directory_list_genre2');

$app->match('artists/artist_directory/all/{type}', "SOSControllers\\ArtistsDirectoryController::showListAll")->bind('artists_directory_list_all');


$app->match('artists/artist_directory/map/{type}/{mapNumber}', "SOSControllers\\ArtistsDirectoryController::showListbyMap")->bind('artists_directory_map_number');

$app->match('artists/artist_directory/search/{type}/{search}', "SOSControllers\\ArtistsDirectoryController::showListbySearchTerm2")->bind('artists_directory_search2');

$app->match('artists/artist_directory/alpha/{type}/{letter}', "SOSControllers\\ArtistsDirectoryController::showListbyLetter")->bind('artists_directory_list_alpha');

//----------------------------
// FOR Artists
//----------------------------
//'REGISTRATION'=>'artists.registration'



if (!is_null($userInfo)){

    $app['artistMenu']= array('General Information'=>'for_artists.home',
        'My Profile'=>'artists.my_profile',
        'Events'=>"artists.events",
        " + First Look Show" =>'first_look_show',
        " + Fashion Show" =>'fashion_show',
        'Calendar'=>'artists.calendar',
        'LOGIN'=>'login',
        'Community Space'=>'community_space',
        'Show My Profile'=>'for_artists.show_my_profile');

} else {
    $app['artistMenu']= array('General Information'=>'for_artists.home','My Profile'=>'artists.my_profile','Events'=>"artists.events"," + First Look Show" =>'first_look_show'," + Fashion Show" =>'fashion_show','Calendar'=>'artists.calendar','LOGIN'=>'login', 'Community Space'=>'community_space');
}

$artist = $app['controllers_factory'];
$artist->get('/', function (Request $request) use ($app){

    $userInfo =$app['session']->get('sos_user_info');
    $app['request'] = $request;

    //$tempMenu = \SOSModels\Menu::$artistMenu;

    return $app['twig']->render('for_artists/index.html.twig',array('side_menu' => $app['artistMenu'], 'user_info'=>$userInfo));
})->bind('for_artists.home');


$artist->match('/login', "SOSControllers\\LoginController::login")->bind('login');
$artist->match('/login/{uid}/{hash}', "SOSControllers\\LoginController::autologin")->bind('autologin');
$artist->match('/logout', "SOSControllers\\LoginController::logout")->bind('logout');
$artist->match('/update_password?uid={uid}&hid={hash}', "SOSControllers\\LoginController::passwordUpdate")->bind('password_update');
$artist->match('/update_password/{uid}/{hash}', "SOSControllers\\LoginController::passwordUpdate")->bind('password_update');
$artist->match('/reset_request', "SOSControllers\\LoginController::reset_request")->bind('password_reset_request');
$artist->match('/my_profile', "SOSControllers\\ProfileUpdateController::my_profile")->bind('artists.my_profile');
$artist->match('/show_my_profile', "SOSControllers\\ArtistProfileController::showMyProfile")->bind('for_artists.show_my_profile');


$artist->match('/update_contact_information', "SOSControllers\\ProfileUpdateController::updateContactInfo")->bind('update_contact_info');
$artist->match('/update_location', "SOSControllers\\ProfileUpdateController::updateLocation")->bind('update_location');
$artist->match('/update_profile', "SOSControllers\\ProfileUpdateController::updateProfileInfo")->bind('update_profile_info');
$artist->match('/update_artist_info', "SOSControllers\\ProfileUpdateController::updateArtistInfo")->bind('update_artist_info');
$artist->match('/update_social_media', "SOSControllers\\ProfileUpdateController::updateSocialMedia")->bind('update_social_media');

$artist->match('/update_images', "SOSControllers\\ProfileUpdateController::updateImages")->bind('update_images');
$artist->match('/update_thumbnails', "SOSControllers\\ProfileUpdateController::updateThumbnails")->bind('update_thumbnails');

$artist->get('/registration_thank_you', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/registration_thank_you.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.registration_thank_you');


$artist->get('/registration', function () use ($app) {
    return $app['twig']->render('for_artists.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.registration');

$artist->get('/events', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/events.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.events');

$artist->get('/calendar', function (Request $request) use ($app) {
    $app['request'] = $request;

    return $app['twig']->render('/for_artists/artist_calendar.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.calendar');


$artist->get('/community_space', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/community_space.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('community_space');

$artist->get('/fashion_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_fashion.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('fashion_show');


$artist->get('/first_look_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_first_look.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('first_look_show');



$artist->get('/FAQ', function () use ($app) {
    return $app['twig']->render('for_artists.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.faq');

$app->mount('/for_artists', $artist);

//----------------------------
// Support
//----------------------------

$support = $app['controllers_factory'];
$supportMenu = array('HOME'=> 'homepage','SUPPORT'=>'support.home','SPONSOR' =>'support.sponsor','VOLUNTEER'=>'volunteer.signup','DONATE'=>'support.donate');

$support->get('/', function (Request $request) use ($app, $supportMenu){
    $app['request'] = $request;
    return $app['twig']->render('/support/supporters.html.twig',array('side_menu' => $supportMenu));
})->bind('support.home');

$support->get('/sponsor',  "SOSControllers\\SponsorController::listing")->bind('support.sponsor');
$support->match('/sponsor_purchase',  "SOSControllers\\SponsorController::purchase")->bind('support.sponsor_purchase');

$support->match('/donate',"SOSControllers\\DonationController::donate")->bind('support.donate');


$app->mount('/support', $support);

//----------------------------
// About
//----------------------------

$about = $app['controllers_factory'];
$app['aboutMenu'] = array('HOME'=> 'homepage', 'ABOUT SOS'=>'about.home','PRESS INFO'=>'about.press_info','NEWS'=>'about.in_the_news', 'SOS IN EDUCATION'=> 'about.education' ,  'HISTORY'=> 'about.history', 'BUDGET'=>'about.sos_budget' ,'CONTACT US'=>'about.contact');

$about->get('/', function (Request $request) use ($app){
    $app['request'] = $request;

    return $app['twig']->render('about/about.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.home');


$about->get('/sos_history', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/history.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.history');

$about->get('/sos_budget', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/sos_budget.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.sos_budget');

$about->get('/sos_in_education', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/education.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.education');

$about->get('/press_info', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/press_info.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.press_info');

$about->get('/contact', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/contact.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.contact');

/*

$about->get('/in_the_news', function () use ($app, $aboutMenu){
    return $app['twig']->render('about/in_the_news.html.twig',array('side_menu' => $aboutMenu));
})->bind('about.in_the_news');
*/

$about->get('/in_the_news',"SOSControllers\\InTheNewsController::listnews")->bind('about.in_the_news');

$app->mount('/about', $about);

//----------------------------
// Volunteer
//----------------------------

$volunteer = $app['controllers_factory'];

$app['volunteerMenu'] = array('HOME'=> 'homepage','ABOUT VOLUNTEERING'=>'volunteer.home', 'VOLUNTEER'=>'volunteer.signup');

$volunteer->get('/', function (Request $request) use ($app){
    $app['request'] = $request;

    return $app['twig']->render('volunteer/index.html.twig',array('side_menu' =>$app['volunteerMenu']));
})->bind('volunteer.home');


$volunteer->get('/thanks', function (Request $request) use ($app){
    $app['request'] = $request;

    return $app['twig']->render('volunteer/thanks.html.twig',array('side_menu' =>$app['volunteerMenu']));
})->bind('volunteer.thanks');


$volunteer->match('/signup/', "SOSControllers\\VolunteerController::signup")->bind('volunteer.signup') ;

$app->mount('/volunteer', $volunteer);


//--------------------------------------
// Paymnet

$app->match('payment/{pid}/{hid}', "SOSControllers\\PaymentController::makePayment")->bind('make_payment');


//-----------------------------------------
// Admin
$app->match('admin_twenty', "SOSControllers\\AdminTwentyController::showTwenty")->bind('twentyAdmin');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {

    if ($app['debug']) {
       return;
    }


    $theCode = substr($code, 0, 3);

    switch ($theCode) {
        case 404:
            $message = 'The requested page could not be found. <p> <a href="/"> try looking from the home page</a>';
            break;
        default:
            $message = 'We are sorry, but something went wrong.';
    }

    // someday we'll make a good 404 page.. but for now..
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($message, $code);
    //return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});



return $app;
