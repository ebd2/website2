<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());

$app['session.db_options'] = array(
    'db_table'      => 'sessions',
    'db_id_col'     => 'sess_id',
    'db_data_col'   => 'sess_data',
    'db_time_col'   => 'sess_time',
    'db_lifteime_col' => 'sess_lifetime'
    );


$app['session.storage.handler'] = function () use ($app) {
    return new PdoSessionHandler(
        $app['pdo'],
        $app['session.db_options']
    );
};

/* This can be used to set the session longer */
// set in php.ini :  session.gc_maxlifetime "3600"

$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    //$twig->addExtension(new Twig_Extensions_Extension_Text());
    $twig->addGlobal('sos_year', '2019');
    $twig->addGlobal('sos_day_year', 'May 5 + 6, 2019');

    return $twig;
});




$app->before(function ($request) {
    $request->getSession()->start();
});


return $app;
