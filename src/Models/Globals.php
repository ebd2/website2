<?php

//global state variable for SOS

namespace SOSModels;

class Globals{

    public static $sql_debug = true;
    public static $debug = true;

    public static $sos_year = "2019";
    public static $sos_financial_year = "2019";
    public static $sos_date = "May 4+5 2019";
    public static $sos_date_with_preview = "May 4+5 2019 - preview May 3";


    public static $social_media_icon_dir = "web/images/social_media_icons";
   /// $app['mapSettings'] = array('bounds'=>'false', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );


// if profile
  // $app['profileFreeze'] = true;

/*
public function __construct()
{




}
*/

}