<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/3/15
 * Time: 12:56 AM
 */

namespace SOSModels;


class ArtistData {

    private $sos_dbo;
    private $imageProfile;
    private $memberID;


    public function __construct (\PDO $dbo , $memberID){
        $this->sos_dbo = $dbo;
        $this->artistProfile = null;
        $this->memberID = $memberID;


    }
/*
    static function init(\PDO $dbo , $memberID){




    return $stuff ? new self(\PDO $dbo , $memberID) : false;
}
*/
    /**
     *
     * return all artists information. cached when called..
     *  use forceRefresh = true to force reload from database.
     *
     * @return null
     *
     */
    public function getArtistInfo($forceRefresh = false){

        if ($this->artistProfile == null || $forceRefresh == true) {

            $sql = "SELECT * from current_active_artists_view v JOIN statement s ON (v.id=s.member_id) WHERE v.id = :member_id";

            $stmt = $this->sos_dbo->prepare($sql);
            $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);

            $stmt->execute();
            $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (empty($row)){
                return null;
            }
            $this->artistProfile = $row[0];

        }

        return $this->artistProfile;

    }






    /*
     *
     *
     *  array (size=3)
    0 =>
      array (size=7)
      'id' => string '3991' (length=4)
      'member_id' => string '592' (length=3)
      'ImageNumber' => string '1' (length=1)
      'Title' => string 'surfer - Hurley pro tour' (length=24)
      'Medium' => string 'photo' (length=5)
      'Dimensions' => string 'photo size' (length=10)
      'filename' => string 'artist_files/artist_images/profile/592-01.jpg' (length=45)
  1 =>
    array (size=7)
     *
     */



    // Returns array of image data

    public function getArtistImagesData(){

        $this->getArtistInfo();
        $oneArtist = $this->artistProfile ;


        if ($this->artistProfile['Expanded_Web'] == 'Y'){
            $numberOfImages = 8;
        } else {
            $numberOfImages = 3;
        }

        $sos_image_path='artist_files/artist_images/profile/';
        $imageArray = array();
        $jpg_number= array( '01','02','03','04','05','06','07','08');

        // lookup imaage details in database

        $sql = "SELECT * from image WHERE member_id = :member_id ";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // index based on image number..
        $imageDetails = array();
        foreach ($rows as $oneImage){
            $imageDetails[($oneImage['ImageNumber'])] = $oneImage;
        }

        for ($i=0; $i< $numberOfImages; $i++){

            $profile_jpg = $sos_image_path  . $oneArtist['member_id'] . '-'.$jpg_number[$i] .'.jpg';
            if (file_exists( $profile_jpg )){

            } else {
                $profile_jpg = $sos_image_path  ."noimage.jpg";
            }
            $indx=$i+1;
            if (array_key_exists($indx,$imageDetails)) {

                $imageDetails[$indx]['filename']= $profile_jpg;
                $imageArray[] = $imageDetails[$indx];

            } else {

                // No data about the image.  Shouldn't happen, but if it does return blanks...
                $imageArray[] = array ( 'filename'=> $profile_jpg, 'id' => '','member_id' =>'', 'ImageNumber' =>$indx,'Title' => '','Medium' => '','Dimensions' => '');
            }

        }

        return $imageArray;

    }


    // the thumbnail image had no other information associated with it.. So just report the name/path and  if the image exists.

    public function getArtistThumbnailData()
    {


        $imageName = "{$this->memberID}.jpg";
        $sos_image_path = 'artist_files/artist_images/splash/';

        $fullFileName = $sos_image_path.''.$imageName;

        if (file_exists( $fullFileName)){
            $fileExists = true;
        } else {
            $imageName ="dummy.gif";
            $fullFileName = $sos_image_path.''.$imageName;
            $fileExists = false;

        }



        return array('image_name'=>$imageName, 'path'=>$sos_image_path, 'filename_and_path'=> $fullFileName   ,'image_exists'=>$fileExists);



    }


    /**
     * given user info, get what informatoin to show.
     *
     *
     */

    public function getUserFeatures()
    {


        $features = array();
        $this->getArtistInfo();

        if (empty($this->artistProfile)){

            return null;
        }

        $feature['expanded_web'] =$this->artistProfile['Expanded_Web'];
        $feature['friday_night'] =$this->artistProfile['FridayNight'];
        $feature['show_business_name'] =$this->artistProfile['BusinessNameOption'];
        $feature['show_organization_description'] =$this->artistProfile['MemberType']>=2?true:false;
        $feature['extended_profile'] =$this->artistProfile['Expanded_Web'];
        $feature['fashion_show'] =$this->artistProfile['fashion_show'];



        // Open Fridays?
        // show group information?
        // extended profile
        // participating in fashion show.
        // participating in artists' choice
        //


        return $feature;




    }

}