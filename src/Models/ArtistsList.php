<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/1/15
 * Time: 11:59 PM
 */

namespace SOSModels;
use Silex\Application;


class ArtistsList
{

    private $sos_dbo;
    private $sort;
    private $lookupView;

    public function __construct(\PDO $dbo)
    {
        $this->sos_dbo = $dbo;
        $this->sort = "PublicLastName, PublicFirstName";
        $this->lookupView = 'current_active_artists_view';
    }

    public function setSort($col)
    {
        $this->sort = $col;
    }


    //resest session
    // for some reason this will log out a user...

    public static function resetSearchIds (Application $app){


        $app['session']->set('searchList', null);
        $app['session']->set('searchName', null);
        $app['session']->set('searchURL', null);

        $app['session']->remove('searchList');
        $app['session']->remove('searchName');
        $app['session']->remove('searchURL');


    }





    public function setOnlyParticipating()
    {
        $this->lookupView = 'current_participating_artists_view';
    }

    //--------------------------------------------------------------
    // Get All
    //

    public function getAll() {
        $sql = "SELECT * from {$this->lookupView} ORDER BY PublicLastName, PublicFirstName";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;

    }


    //--------------------------------------------------------------
    // Get By Random
    //

    public function getRandom($number) {
        $sql = "SELECT * from {$this->lookupView} ORDER BY RAND() Limit ?";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(1, $number, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        //shuffle ($all_rows);
        return $all_rows;

    }

    //--------------------------------------------------------------
    // Get By Map Number
    //

    public function getByMapNumber($mapNumber) {
        $mapNumber = (int)$mapNumber;
        if (! is_int($mapNumber)) {
            // don't allow access to arbitrary columns of database table!
            print 'Not integer.  map numbner must be numeric';
        }

        $sql = "SELECT * FROM current_participating_artists_view where map_number = :mapnum ORDER BY $this->sort";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(':mapnum', $mapNumber, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;
    }

    //--------------------------------------------------------------
    // Get By Letter
    //

    public function getByLetter($letter) {
        $pattern = $this->sos_dbo->quote($letter . "%");

        // remove the from return order for business names

        $the_pattern = $this->sos_dbo->quote("The " . $letter . "%");

        $sql = "(SELECT *, PublicLastName as sortKey "
            . "FROM current_active_artists_view WHERE PublicLastName like $pattern ) UNION ";

        $sql .= "(SELECT *, TRIM(LEADING 'The ' FROM BusinessName) as sortKey "
            . "FROM current_active_artists_view "
            . "WHERE (BusinessName like $pattern OR BusinessName like $the_pattern) "
            . "HAVING sortKey like $pattern) " ;
        $sql .= "ORDER BY sortKey, PublicFirstName";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;
    }
    //--------------------------------------------------------------
    // Get list by Genre (aka Medium)
    //

    public function getByGenre($genre)  {

        if (! isset(\SOSModels\artistsGenreList::$column_name_to_genre_name[$genre] )) {
            // don't allow access to arbitrary columns of database table!
            return 'No Such type';
        }

        $sql = "SELECT * FROM {$this->lookupView} where `$genre` = 'Y' ORDER BY $this->sort";
        $stmt = $this->sos_dbo->prepare($sql);
        //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;
    }

    //--------------------------------------------------------------
    // Get Search Term

    public function getBySearchTerm($search)
    {

        $nameList = preg_split('/\s+/', $search);
        $pattern0 = $this->sos_dbo->quote("%" . $nameList{0} . "%");
        if (isset ($nameList{1}) && $nameList{1} != "") {
            $pattern1 = $this->sos_dbo->quote("%" . $nameList{1} . "%");
            $sql = <<<EOL
     SELECT * FROM current_active_artists_view where
        PublicFirstName like $pattern0
     or PublicFirstName like $pattern1
     or PublicLastName like $pattern0
     or PublicLastName like $pattern1
     or building_name  like $pattern0
     or building_name  like $pattern1
     or Website  like $pattern0
     or Website  like $pattern1
     or street_address  like $pattern0
     or street_address  like $pattern1
     or address_details  like $pattern0
     or address_details  like $pattern1
     or ShortDescription  like $pattern0
     or ShortDescription  like $pattern1
     or BusinessName  like $pattern0
     or BusinessName  like $pattern1

EOL;
        } else {
            $sql = <<<EOL
      SELECT * from current_active_artists_view where
        PublicFirstName like $pattern0
     or PublicLastName like $pattern0
     or building_name  like $pattern0
     or Website  like $pattern0
     or street_address  like $pattern0
     or address_details  like $pattern0
     or ShortDescription  like $pattern0
     or BusinessName  like $pattern0

EOL;
        }

        $stmt = $this->sos_dbo->prepare($sql);
        //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;


    }






    //--------------------------------------------------------------
    // listToJson
    //
    // list to json format for maps

    public function listToJson ($participants, $mapnum_url, $profile_url, $attrs=null ){

        //public function getParticipantsMapDataJson (){



            $json = '{"markers":['; // start json

            $markerArray = array();  // array indexed by map numbers.

            foreach ($participants as $oneRow){

                $mapNumber= htmlentities($oneRow['map_number']);




                $address_plus2= htmlentities($oneRow['street_address']) ." , Somerville MA ";
                $address_plus3 = str_replace(" ", "+",$address_plus2 );
                $address_plus  = str_replace("#", " ",$address_plus3 );

                $google_line= '<a '. $attrs . '  href="http://maps.google.com/maps?f=q&hl=en&geocode=&q='.$address_plus .'"> <em> [Directions (Google)] </em></a>';
                $business_name = $oneRow['BusinessName'];
                if ($business_name != '') {
                    $business_name = " (".htmlentities($business_name).")";
                }
                $link_to_profile = '<a ' . $attrs . ' href="/'.$profile_url. htmlentities($oneRow['id']).'"> '.htmlentities($oneRow['PublicFirstName']).' ' .htmlentities($oneRow['PublicLastName']).'</a>'.$business_name;


                if (!array_key_exists($mapNumber,$markerArray )){
                    $temp_array= array();
                    $mapnum_link = "<a " . $attrs ."  href=\"/{$mapnum_url}{$oneRow['map_number']}\"><b>Map Number:  #{$oneRow['map_number']}</b></a>";
                    $street_address = $oneRow['street_address'];
                    if ($oneRow['building_name'] != '') {
                        $street_address = $oneRow['building_name'] . ", " . $street_address;
                    }
                    $temp_array['latitude'] = htmlentities($oneRow['lat']);
                    $temp_array['longitude'] = htmlentities($oneRow['lng']);
                    $temp_array['title'] = htmlentities($street_address);
                    $temp_array['content'] = "$mapnum_link<br><b><em>".htmlentities($street_address)."</em></b><br>".$google_line.'<br>'.$link_to_profile ;
                    $temp_array['map_number']= htmlentities($oneRow['map_number']);
                    $temp_array['map_icon'] ="";

                    $markerArray[$mapNumber]=$temp_array;

                } else {


                    $markerArray[$mapNumber]['content'] .= ',<br>'.$link_to_profile  ;
                }

// If the map number is duplicate do nothing (if listing all artists names in the comment do that here....

            }


            $json = '{"markers":['; // start json

            foreach ($markerArray as $oneRow){
                $json .= json_encode($oneRow) .',' ;
            }

            $json .= ']}';  // End json


            return ($json);

    }


}