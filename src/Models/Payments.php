<?php
namespace SOSModels;

// This is from the older SOS site.. But it generates a list of artists as JSON... So we keep.
// Probably overkill, but we keep.

use \SOS\SQLHelper;

class Payments {

    public function __construct(\PDO $dbo) {
        $this->sos_dbo = $dbo;
    }


    private function generateHash($paymentInfo) {

	   // use the payment_id
	   $number= $paymentInfo['payment_id'] * 43 -8;

	   $returnValue = sha1("{$number}asldkfje!345rtp8awht!aw6th{$number}324!");

	   return $returnValue;

   }

    public function get_blank_payment() {

        $currentDate = Date('Y-m-d H:i:s');
        $year = Date('Y');
        $blankpayment = array (
            'member_id'=>'',   //member_id
            'date_posted'=> $currentDate,
            'type'=> '',
            'transaction_date'=> $currentDate,
            'transaction_amount'=> 0,
            'payment_info'=> '',
            'payment_confirmed'=> 'N',
            'item_description'=> '',
            'payment_type'=> '',
            'payer_name'=> '',
            'payer_email'=> '',
            'donation_amount'=> '',
            'financial_year'=> $year,
            'pay_later'=> 'N'


        );


        return $blankpayment;

    }



    public function add_payment($paymentData) {
/*
        payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL COMMENT 'member id responsible for payment',
  `date_posted` datetime DEFAULT NULL,
  `type` enum('membership','donation','sponsorship','other') NOT NULL DEFAULT 'other',
  `transaction_date` datetime DEFAULT NULL,
  `transaction_amount` float DEFAULT NULL,
  `payment_info` varchar(90) DEFAULT NULL,
  `payment_confirmed` enum('Y','N') DEFAULT NULL,
  `item_description` varchar(350) DEFAULT NULL,
  `payment_type` varchar(40) DEFAULT NULL,
  `payer_name` varchar(100) DEFAULT NULL,
  `payer_email` varchar(50) NOT NULL,
  `donation_amount` int(11) NOT NULL DEFAULT '0',
  `financial_year` int(11) DEFAULT NULL,
  `pay_later` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'payment issued to be paid later.  Basically a PO',

*/

        $sql ='INSERT INTO `payments` ( `id`, `date_posted`, `type`, `transaction_date`, `transaction_amount`, `payment_info`, `payment_confirmed`, `item_description`, `payment_type`, `payer_name`, `payer_email`, `donation_amount`, `financial_year`,`pay_later` ) VALUES (:id, :date_posted,  :type,  :transaction_date,  :transaction_amount, :payment_info,  :payment_confirmed,  :item_description,  :payment_type, :payer_name,  :payer_email,  :donation_amount,  :financial_year, :pay_later);';



        // sql_name(form name) => :name
        $paramsToBind = array(
            'member_id'=>'id',   //member_id
            'date_posted'=> 'date_posted',
            'type'=> 'type',
            'transaction_date'=> 'transaction_date',
            'transaction_amount'=> 'transaction_amount',
            'payment_info'=> 'payment_info',
            'payment_confirmed'=> 'payment_confirmed',
            'item_description'=> 'item_description',
            'payment_type'=> 'payment_type',
            'payer_name'=> 'payer_name',
            'payer_email'=> 'payer_email',
            'donation_amount'=> 'donation_amount',
            'financial_year'=> 'financial_year',
            'pay_later'=> 'pay_later'
        );


        $stmt = \SOS\SQLHelper::bindSQL($this->sos_dbo, $sql, $paramsToBind, $paymentData);


        if ($stmt->execute()){
            $newInsertid = $this->sos_dbo->lastInsertId();
        } else {
            $newInsertid = null;
        }


        return $newInsertid;
    }


    public function getPayment ($pid, $hid) {




        /*
                payment_id` int(11) NOT NULL AUTO_INCREMENT,
          `id` int(11) DEFAULT NULL COMMENT 'member id responsible for payment',
          `date_posted` datetime DEFAULT NULL,
          `type` enum('membership','donation','sponsorship','other') NOT NULL DEFAULT 'other',
          `transaction_date` datetime DEFAULT NULL,
          `transaction_amount` float DEFAULT NULL,
          `payment_info` varchar(90) DEFAULT NULL,
          `payment_confirmed` enum('Y','N') DEFAULT NULL,
          `item_description` varchar(350) DEFAULT NULL,
          `payment_type` varchar(40) DEFAULT NULL,
          `payer_name` varchar(100) DEFAULT NULL,
          `payer_email` varchar(50) NOT NULL,
          `donation_amount` int(11) NOT NULL DEFAULT '0',
          `financial_year` int(11) DEFAULT NULL,
          `pay_later` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'payment issued to be paid later.  Basically a PO',

        $FORM DATA
  'first_name' => string 'Aram' (length=4)
  'last_name' => string 'Comjean' (length=7)
  'email' => string 'acomjean2@gmail.com' (length=19)
  'phone' => string '6173310156' (length=10)
  'street_address' => string '1716 cambridge st #11' (length=21)
  'city' => string 'cambridge' (length=9)
  'state' => string 'Massachusetts' (length=13)
  'zip_code' => string '02138' (length=5)
  'amount' => float 25
  'comments' => null



        */

        $sql = "SELECT * FROM payments where payment_id = :payment_id";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":payment_id", $pid, \PDO::PARAM_STR);
        $stmt->execute();
        $paymentInfo = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (empty($paymentInfo)){
            echo "could not locate payment";
            die();

        } else {
            $hash = $this->generateHash($paymentInfo);
            if ($hid != $hash){
                echo "Values don't match: <!-- $hash -->";
                exit();
            }

        }




        return $paymentInfo;

    }



    public function getPaymentHash ($pid) {




        /*
                payment_id` int(11) NOT NULL AUTO_INCREMENT,
          `id` int(11) DEFAULT NULL COMMENT 'member id responsible for payment',
          `date_posted` datetime DEFAULT NULL,
          `type` enum('membership','donation','sponsorship','other') NOT NULL DEFAULT 'other',
          `transaction_date` datetime DEFAULT NULL,
          `transaction_amount` float DEFAULT NULL,
          `payment_info` varchar(90) DEFAULT NULL,
          `payment_confirmed` enum('Y','N') DEFAULT NULL,
          `item_description` varchar(350) DEFAULT NULL,
          `payment_type` varchar(40) DEFAULT NULL,
          `payer_name` varchar(100) DEFAULT NULL,
          `payer_email` varchar(50) NOT NULL,
          `donation_amount` int(11) NOT NULL DEFAULT '0',
          `financial_year` int(11) DEFAULT NULL,
          `pay_later` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'payment issued to be paid later.  Basically a PO',

        $FORM DATA
  'first_name' => string 'Aram' (length=4)
  'last_name' => string 'Comjean' (length=7)
  'email' => string 'acomjean2@gmail.com' (length=19)
  'phone' => string '6173310156' (length=10)
  'street_address' => string '1716 cambridge st #11' (length=21)
  'city' => string 'cambridge' (length=9)
  'state' => string 'Massachusetts' (length=13)
  'zip_code' => string '02138' (length=5)
  'amount' => float 25
  'comments' => null



        */
        $hash = null;
        $sql = "SELECT * FROM payments where payment_id = :payment_id";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":payment_id", $pid, \PDO::PARAM_STR);
        $stmt->execute();
        $paymentInfo = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!empty($paymentInfo)){
            $hash = $this->generateHash($paymentInfo);

        }




        return $hash;

    }




}
