<?php
namespace SOSModels;

// This is from the older SOS site.. But it generates a list of artists as JSON... So we keep.
// Probably overkill, but we keep.

use \SOS\SQLHelper;

class Location {

    public function __construct(\PDO $dbo) {
        $this->sos_dbo = $dbo;
        $this->debug = \SOSModels\Globals::$debug;
        $this->debug = false;
    }



    public function getLocationsWith4More(){

        // get locations with the number of locations subscribed to them.
        $sql = 'select count(*) as `the_count`, location_id from profile_with_location where `location_id`>0  group by `location_id` order by `the_count` desc;';


        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();

        if (empty($results)){
            return null;
        }


        $locationListMoreThan4 = array();

        foreach ($results as $oneID){
            if ($oneID['the_count']>4){
                $id =  $oneID['location_id'];
                $locationListMoreThan4["$id"] = $this->getLocation($id);

            }
        }

        return $locationListMoreThan4;
    }



    public function update_location ($paymentData) {


        $sql ='INSERT INTO `location` ( `id`, `date_posted`, `type`, `transaction_date`, `transaction_amount`, `payment_info`, `payment_confirmed`, `item_description`, `payment_type`, `payer_name`, `payer_email`, `donation_amount`, `financial_year`,`pay_later` ) VALUES (:id, :date_posted,  :type,  :transaction_date,  :transaction_amount, :payment_info,  :payment_confirmed,  :item_description,  :payment_type, :payer_name,  :payer_email,  :donation_amount,  :financial_year, :pay_later);';



        // sql_name(form name) => :name
        $paramsToBind = array(
            'member_id'=>'id',   //member_id
            'date_posted'=> 'date_posted',
            'type'=> 'type',
            'transaction_date'=> 'transaction_date',
            'transaction_amount'=> 'transaction_amount',
            'payment_info'=> 'payment_info',
            'payment_confirmed'=> 'payment_confirmed',
            'item_description'=> 'item_description',
            'payment_type'=> 'payment_type',
            'payer_name'=> 'payer_name',
            'payer_email'=> 'payer_email',
            'donation_amount'=> 'donation_amount',
            'financial_year'=> 'financial_year',
            'pay_later'=> 'pay_later'
        );


        $stmt = \SOS\SQLHelper::bindSQL($this->sos_dbo, $sql, $paramsToBind, $paymentData);


        if ($stmt->execute()){
            $newInsertid = $this->sos_dbo->lastInsertId();
        } else {
            $newInsertid = null;
        }


        return $newInsertid;
    }




    public function getLocationStreetName ($streetNumber, $streetName) {



        $sql = "SELECT * FROM locations where street_number = :street_number and street_name = :street_name";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":street_number", $streetNumber, \PDO::PARAM_STR);
        $stmt->bindValue(":street_name", $streetName, \PDO::PARAM_STR);


        if ($stmt->execute()) {
            $resultData = $stmt->fetch(\PDO::FETCH_ASSOC);

        } else {
            if (\SOSModels\Globals::$sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());
            }
            $resultData = null;
        }

        return $resultData;

    }


    /**
     * Given a location ID get the details
     *
     * @param $locationID
     * @return mixed|null
     */


    public function getLocation($locationID) {

        $sql = "SELECT * FROM locations where location_id = :location_id";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":location_id", $locationID, \PDO::PARAM_STR);


        if ($stmt->execute()) {
            $resultData = $stmt->fetch(\PDO::FETCH_ASSOC);

        } else {
            if (\SOSModels\Globals::$sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());
            }
            $resultData = null;
        }

        return $resultData;

    }


    public function completeStreetName ($partialStreetName) {


        $streetName = "{$partialStreetName}%";

        $sql = "SELECT * FROM location_streets where street_name like :street_name";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":street_name", $streetName, \PDO::PARAM_STR);


        if ($stmt->execute()) {
            $resultData = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        } else {
            if (\SOSModels\Globals::$sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());
            }
            $resultData = null;
        }

        return $resultData;

    }



    public function saveMemberLocation ($memberID, $formData ){

        $locationID = $formData['studio_picker'];
        $streetNumber = $formData['street_number'];
        $streetName = $formData ['street_name'];
        $buildingName = $formData['building_name'];
        $hc = $formData['accessible'];


        if ($this->debug){
            echo"memeberID:{$memberID} locationID {$locationID} Street #: {$streetNumber},  Street Name: $streetName,   $buildingName <br>";
        }



        $streetNumber = trim($streetNumber);
        $streetName = trim($streetName);

        // --------------------------------------------------
        // --Get Existing locations from database if there --
        // --------------------------------------------------

        // If $locationID from the form is numeric then the user selected a pre-set location.  Just get it.

        if (is_numeric($locationID)){

            $insertID = $locationID;

            // look for street address, if it does put the location_id into the profil
            $sql ="SELECT * FROM `locations` WHERE location_id = :location_id";
            $stmt = $this->sos_dbo->prepare ($sql);
            $stmt->bindParam (':location_id',$locationID, \PDO::PARAM_STR );
            $stmt->execute();
            $match = $stmt->fetchAll();

            if (empty ($match)){
                return null;
            }

            // override the Accessibility if the location is Y or N..
            $loc_accessible =   $match[0]['handicap_accessible'];
            if (($loc_accessible=='Y') || ($loc_accessible=='N')) {
                $hc = $loc_accessible;
                // other possible value is 'U' for unknown,
                // in which case we accept the user's choice
            }



            // Custom Address, try to find

        } else {


            // look for street address, if it does put the location_id into the profil
            $sql ="SELECT * FROM `locations` WHERE city='Somerville' AND `street_number` LIKE :street_number AND `street_name` LIKE :street_name";
            $stmt = $this->sos_dbo->prepare ($sql);
            $stmt->bindParam (':street_number',$streetNumber, \PDO::PARAM_STR );
            $stmt->bindParam (':street_name',$streetName, \PDO::PARAM_STR );
            $stmt->execute();
            $match = $stmt->fetchAll();

            if ($this->debug)
                var_dump ($match);

            // 1 The Address Exists !

            if (!empty($match)) {
                $insertID=$match[0]['location_id'];

                // override the users choice of accessability if the location already has it defined.

                $loc_accessible =   $match[0]['handicap_accessible'];
                if (($loc_accessible=='Y') || ($loc_accessible=='N')) {
                    $hc = $loc_accessible;
                    // other possible value is 'U' for unknown,
                    // in which case we accept the user's choice
                }

            // 2 New Address

            } else {

                // save the street address in the locations table and store the new location id into the profile table
                $sql = 'INSERT INTO  `locations` (`location_id`, `building_name`,  `street_number`, `street_name`, `street_address`, `city`, `state`, `zip`, `notes`, `lat`, `lng`, `map_number`, `map_coordinates`) VALUES (NULL, :building_name, :street_number,  :street_name, :street_address, "Somerville", "MA", "", NULL, NULL, NULL, NULL, NULL);';
                $streetAddress ="{$streetNumber} {$streetName}";
                $stmt = $this->sos_dbo->prepare ($sql);
                $stmt->bindParam (':street_number',$streetNumber, \PDO::PARAM_STR );
                $stmt->bindParam (':street_name',$streetName, \PDO::PARAM_STR );
                $stmt->bindParam (':street_address',$streetAddress, \PDO::PARAM_STR );
                $stmt->bindParam (':building_name', $buildingName, \PDO::PARAM_STR );



                if ($stmt->execute()) {

                } else {
                    if (\SOSModels\Globals::$sql_debug) {
                        echo "\nPDO::errorInfo():\n";
                        print_r($stmt->errorInfo());
                    }
                }

                $insertID = $this->sos_dbo->lastInsertId();

                //
                $gmapObj = new \SOS\GoogleMaps();
                $key=null; // Use default key
                $gmapObj->sos_geocode_key($this->sos_dbo, $key, $insertID, $streetNumber.' '.$streetName.', Somerville massachusetts');

            }

        }


        // update the profile with the location id and other fields..


        $sql = "UPDATE `profile` SET `location_id` = :location_id, `HandicapAccessible` = :access, `home_studio` = :home_studio, `address_details` = :address_details WHERE `profile`.`member_id` = :member_id;";
        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':location_id',$insertID, \PDO::PARAM_STR );
        $stmt->bindParam (':member_id',$memberID, \PDO::PARAM_STR );
        $stmt->bindParam (':home_studio',$formData['home_studio'], \PDO::PARAM_STR );
        $stmt->bindParam (':address_details',$formData['address_details'], \PDO::PARAM_STR );
        $stmt->bindParam (':access',$hc, \PDO::PARAM_STR );
        $locId = $stmt->execute();

        return $insertID;

    }



    // SAVE LOCATION Reference
/*
    private function saveMemberLocationOriginal ($memberID, $locationID, $streetAddress, $buildingName){

        if ($this->debug){
            echo"memeberID:{$memberID} locationID {$locationID} Street: {$streetAddress}, $buildingName <br>";
        }
        if (is_numeric($locationID)){
            // insert location number into the artists profile table
            $sql = "UPDATE `profile` SET `location_id` = :location_id WHERE `profile`.`member_id` = :member_id;";
            $stmt = $this->sos_dbo->prepare ($sql);
            $stmt->bindParam (':location_id',$locationID, PDO::PARAM_STR );
            $stmt->bindParam (':member_id',$memberID, PDO::PARAM_STR );
            $stmt->execute();
            $result = $stmt->fetchAll();
            $insertID = $locationID;
        } else {


            // look for street address, if it does put the location_id into the profil
            $sql ="SELECT * FROM `locations` WHERE city='Somerville' AND `street_address` LIKE :streetAddr";
            $stmt = $this->sos_dbo->prepare ($sql);
            $stmt->bindParam (':streetAddr',$streetAddress, PDO::PARAM_STR );
            $stmt->execute();
            $match = $stmt->fetchAll();

            if ($this->debug)
                var_dump ($match);

            if (!empty($match)) {
                $insertID=$match[0]['location_id'];
                if ($this->debug)
                    echo "location match = {$insertID} <br>";

            } else {

                // save the street address in the locations table and store the new location id into the profile table
                $sql = 'INSERT INTO  `locations` (`location_id`, `building_name`, `street_address`, `city`, `state`, `zip`, `notes`, `lat`, `lng`, `map_number`, `map_coordinates`) VALUES (NULL, :building_name, :street_address, "Somerville", "MA", "", NULL, NULL, NULL, NULL, NULL);';

                $stmt = $this->sos_dbo->prepare ($sql);
                $stmt->bindParam (':street_address',$streetAddress, PDO::PARAM_STR );
                $stmt->bindParam (':building_name', $buildingName, PDO::PARAM_STR );
                $id = $stmt->execute();

                $insertID = $this->sos_dbo->lastInsertId();
            }

            // update the profile with the location id

            $sql = "UPDATE `profile` SET `location_id` = :location_id WHERE `profile`.`member_id` = :member_id;";
            $stmt = $this->sos_dbo->prepare ($sql);
            $stmt->bindParam (':location_id',$insertID, PDO::PARAM_STR );
            $stmt->bindParam (':member_id',$memberID, PDO::PARAM_STR );
            $locId = $stmt->execute();
            $result = $stmt->fetchAll();

        }

        return $insertID;

    }

*/





}
