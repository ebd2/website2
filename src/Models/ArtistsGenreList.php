<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/6/15
 * Time: 9:51 PM
 */

namespace SOSModels;


class ArtistsGenreList
{

    public function __construct(\PDO $dbo)
    {
        $this->sos_dbo = $dbo;


        // these are the only field that can be updated by this class.
        // The index key is the table that can be updated
        // in this case only one table is involved, but for commonality..

        $keys= array_keys (self::$column_name_to_genre_name);

        $this->profileUpdateFields = array('profile'=>array(
            'artist_statement',
            'HandicapAccessible',
            'ShortDescription',
            'PublicLastName',
            'PublicFirstName',
            'BusinessName',
            'OrganizationDescription',
            'address_details',
            'use_images_for_promotion',
            'home_studio'),

        );

        $this->tableToWhereColumn = array ('profile'=>'member_id');


    }
    /* this array maps a 'Y/N' genre column name in the member table
        to the genre name that we want to display on an artist's profile page */




    public static $column_name_to_genre_name = array(
        'Books' => 'Books + Paper',
        'Collage' => 'Collage',
        'Drawing' => 'Drawing',
        'Fiber' => 'Fiber + Textiles',
        'Furniture' => 'Furniture',
        'Glass' => 'Glass + Mosiacs',
        'Graphic' => 'Graphic Design',
        'Installation' => 'Installation',
        'Jewelry' => 'Jewelry + Beads',
        'Mixed_Media' => 'Mixed-Media',
//     'Multimedia' => 'Multimedia',  removed for sos2015
        'Painting' => 'Painting',
        'Photography' => 'Photography',
        'Pottery' => 'Pottery',
        'Printmaking' => 'Printmaking',
        'Sculpture' => 'Sculpture',
        'Video' => 'Video',
        'Other_Media' => 'Other');





    /* given a database row from the member table,
       return a comma-space separated list of this member's art genres.
       If the Other_Media column is 'Y', append the artist's "Other" string (if any) */

    public function getGenreText ($data) {
        $mtype = '';
        foreach (self::$column_name_to_genre_name as $column => $genre) {
            if ($data[$column]=='Y') {$mtype .= "$genre, ";}
        }
        $mtype = rtrim($mtype, ", ");  // strip trailing separator
        if (substr($mtype, -5) == "Other") {
            $other = $data['Other'];
            if ($other != '') {$mtype .= ": $other";}
        }
        return $mtype;
    }

    /*
        array (size=3)
        0 => string 'Books' (length=5)
        1 => string 'Graphic' (length=7)
        2 => string 'Sculpture' (length=9)
    */

    public function getGenreFormCheckboxes ($profileData){

        $genreArray = array();

        foreach (self::$column_name_to_genre_name as $column => $genre) {
            if ($profileData[$column] == 'Y'){
                $genreArray[] = $column;
            }
        }
        return ($genreArray);
    }


    /* given an input array ('book', 'painting'...);
    generate an array with all the genre's with "Y' or "no"  eg: ('Books'=>'Y', 'Collage'=>'N'....)
    */


    public function createGenreYNArray ($genreArray, $otherString, $dbo){


        $values = array();

        foreach (self::$column_name_to_genre_name as $column => $genre) {
            $values[$column] = in_array($column, $genreArray)? 'Y':'N';
        }


        $values['Other']= $otherString;
        return $values;
    }



    public function updateGenreData ($newData,  $originalData, $memberID, $dbo) {

        // get difference between existing and new
        $status = array('status' => 'pass', 'messages' => array());

        foreach ($this->profileUpdateFields as $tablename => $fieldNames) {

            // add the "Other" field name to the form.
            $fieldNames = array_keys(self::$column_name_to_genre_name);
            $fieldNames[] = 'Other';

            if ($newData['Other_Media'] == 'N' && !empty($newData['Other'])) {

                $newData['Other'] = '';
                $status['messages'][] = "Set Other Media text field to blank";
            }


            $changedFields = \SOS\SQLHelper::findChangesInArray($fieldNames, $originalData, $newData);

            // Check the other field to make sure its coeherent.

            //var_dump ($changedFields);
            // create SQL to update.

            $fieldUpdateArray = array();
            foreach ($changedFields as $fieldName => $value) {
                $fieldUpdateArray[] = "`{$fieldName}` = :{$fieldName}";
            }

            $fieldNamesSQL = implode(',', $fieldUpdateArray);

            // for this case the update variable names are the same as the names that came into the form
            // so create an array where the keys = the values.  eg ("one"=>"one", "two"=>"two")...

            // before we send to the binding function, add the profile id.


            $colsToBind = array_keys($changedFields);

            // get the column name used in the where clause
            $whereColName = $this->tableToWhereColumn[$tablename];
            $colsToBind[$whereColName] = $whereColName;

            $paramsToBind = array_combine($colsToBind, $colsToBind);

            if (!empty($changedFields)) {
                $changedFields[$whereColName] = $memberID;


                // set up the base SQL
                $sql = "update  `{$tablename}` set {$fieldNamesSQL} WHERE $whereColName= :{$whereColName}";

                // bind the input fields to the sql
                $stmt = \SOS\SQLHelper::bindSQL($this->sos_dbo, $sql, $paramsToBind, $changedFields);

                // run the insert query
                if ($stmt->execute()) {
                    $status['messages'][] = "+++ Updated OK";
                } else {
                    $status['status'] = "failed";
                    $status['messages'][] = "+++ Updated Failed";
                    echo "\nPDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                }
            }
        }

        return $status;

    }

}