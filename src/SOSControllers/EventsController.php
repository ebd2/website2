<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/2/15
 * Time: 12:36 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class EventsController {


    public function showEventPage($event_name, Request $request, Application $app) {

        $eventNameToTemplate = array("2019_fashion_show"=>"2019_fashion_show.html.twig");

        if (!isset($eventNameToTemplate[$event_name])){

            return "no such event..";
        }

        $templateName = "events_fullpage/".$eventNameToTemplate[$event_name];


        $app['request'] = $request;

        return $app['twig']->render($templateName,array());


    }

}