<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArtistsDirectoryHomeController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }

    public function generatePage(Request $request, Application $app){
        $app['request'] = $request;

        //echo $id ."<br>";
      //  $artistProfile = new \SOSModels\ArtistData($app['pdo'], $id);

        //$profile = $artistProfile->getArtistInfo();
        //$images =  $artistProfile->getArtistImagesData();
        //$artistData=null;

        $searchForm = new \SOSForms\SearchArtistForm();

        $form = $searchForm->getForm($app);

        // Check form if submitted


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                $returnPage = $searchForm->processFormData($formData, $app);

                return $returnPage;
            }
        }

        $genreList = \SOSModels\ArtistsGenreList::$column_name_to_genre_name;

        //$app['url_generator']->generate('artists_directory_list_alpha', array('type'=> 'list', 'letter'=>$letter))

        $letters = range('A','Z');

        return  $app['twig']->render('artists/artist_list_home.html.twig',array('genre_list'=>$genreList, 'main_menu' => \SOSModels\Menu::$visitMenu, 'search_letters'=>$letters, 'search_form'=>$form->createView()));



    }

}