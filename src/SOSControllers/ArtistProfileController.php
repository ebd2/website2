<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


class ArtistProfileController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }

    public function showProfile($id, Request $request, Application $app){

        $app['request'] = $request;
        // lets see if the session data made it to here.
        // we never know...
        $searchList =$app['session']->get('searchList');
        $searchName =$app['session']->get('searchName');
        $searchURL =$app['session']->get('searchURL');

        $key = null;
        $searchNav = array();

        // we have a list of items.  Find the location of the current artists.  generate the next/previous first/last links
        if (!empty($searchList) && count($searchList) >1) {
            $key = array_search($id, $searchList);

            $searchNav['first'] = $searchList[0];
            $searchNav['last'] = $searchList[count($searchList)-1];

            // if at beginning of list we don't have a previous
            
            if ($key!=0){
                $searchNav['previous'] =$searchList[$key-1];
            } else {
                $searchNav['previous'] = null;
            }


            // if at end of list we don't have a next
            
            if ($key < count($searchList)-1){
                $searchNav['next'] = $searchList[$key+1];
            } else {
                $searchNav['next'] = null;
            }

            // get random element from list

            if (count($searchList) > 1) {
                $searchNav['random'] =  $searchList[array_rand($searchList)];
            } else {
                $searchNav['random'] = null;
            }
            $k = $key+1;
            $searchNav['counter'] = "( {$k} of ".count($searchList) .')';
            $searchNav['url'] = $searchURL;
        }



        $artistProfile = new \SOSModels\ArtistData($app['pdo'], $id);



        $profile = $artistProfile->getArtistInfo();
        $images =  $artistProfile->getArtistImagesData();
        $artistThumbnailData = $artistProfile->getArtistThumbnailData();

        $socialMediaObj = new \SOSModels\ProfileSocialMedia($app['pdo']);
        $socialMedia = $socialMediaObj->getSocailMedia($id);

        if (empty($artistProfile->artistProfile)){

            $app['session']->getFlashBag()->add('danger', 'Artist Profile Not Found for artist:'.$id);
            return $app->redirect($app["url_generator"]->generate("artists_directory",  array()));


        }


        $artistData=null;

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $artistsListObj->setSort('map_number');
        $mapNumber = $profile['map_number'];

        $artists = $artistsListObj->getByMapNumber($mapNumber);
        $mapJson = $artistsListObj->listToJson($artists, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');
        

        $mapSettings = array('bounds'=>'false', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>$profile['lat'].','.$profile['lng'] );

        return $app['twig']->render('artists/artist_profile.html.twig',array('map_json'=>$mapJson,
            'artist_data'=>$profile,
            'image_data'=> $images,
            'thumbnail_data'=>$artistThumbnailData,
            'map_settings'=> $mapSettings,
            'search_nav' => $searchNav,
            'social_data' => $socialMedia,
            'search_name' => $searchName,
            'main_menu' => $app['artistsMenu']));

        //return new Response("Actor Action respose (bar)".$id);


    }


    /**
     * Return the logged in users profile
     *
     * @param Request $request
     * @param Application $app
     * @return string
     *
     *
     */

    public function showMyProfile( Request $request, Application $app){

        $app['session']->getFlashBag()->add('danger', 'This is your user profile');


        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)){
            return "User Not Logged in";
        }

        $member_id= $userInfo ['id'] ;
       // $app['session']->set('searchList')=null;

        // clear sessions, because this is the artists user profile
        $app['session']->remove('searchList');
        $app['session']->remove('searchName');
        $app['session']->remove('searchURL');

        // create path and redirect;
        return $app->redirect($app["url_generator"]->generate("artists_profile",  array('id' => $member_id)));



    }

}