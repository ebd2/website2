<?php

namespace SOSControllers;
//use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Silex\Application;


/*'sos_user_info'
    array (size=9)
      'id' => string '592' (length=3)
      'EmailAddress' => string 'acomjean2@gmail.com' (length=19)
      'epassword' => string '342b9c30e9464068c4dcf69ead0f98b1' (length=32)
      'password' => string '$2y$10$VPDxTIeDEUiODTNDafPkAOWXuxHSycTGVv9yDmiNZUMNoJFcvr9J2' (length=60)
      'Active' => string 'N' (length=1)
      'MemberType' => string '2' (length=1)
      'Expanded_Web' => string 'N' (length=1)
      'Pending' => string 'Y' (length=1)
      'Pending_Date' => string '2015-09-28 21:16:46' (length=19)
*/
class ProfileUpdateController
{

    public function bar($id, Request $request, Application $app)
    {

        return new Response("Actor Action respose (bar)" . $id);


    }


    //----------------
    // Profile Summary Page  (/my_profile)
    //----------------

    public function my_profile(Request $request, Application $app)
    {
        $app['request'] = $request;
        $errorArray = array();
        $infoArray = array();
        $warningArray = array();
        $loginOK = false;

        $menu = $app['artistMenu'];


        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {

            return "User not logged in";
        }

        // get user profile

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);

        $profile = $artistData->getArtistInfo();
        $images = $artistData->getArtistImagesData();

        // try to get the image list from another source.

        $images2 = $artistData->getArtistImagesData();
        $thumbnail = $artistData->getArtistThumbnailData();
        $thumbnail_path = $thumbnail['filename_and_path'];

        //  print "image2<br>";

        // var_dump ($images2);

        // profile returns as null if user not active.
        if (is_null($profile)) {

            return "<br>User has no profile";
        }

        $artistInfoCols = array('PublicFirstName' => 'Name',
            'BusinessName' => 'Business Name',
            'ShortDescription' => 'Short Description',
            'use_images_for_promotion' => 'SOS use images for promotion',
            'home_studio' => 'My studio is in my home',
            'FridayNight' => 'Open Friday Night',
            'FashionInfo' => 'Request Fashion Show Info');

        $mapbokInfoCols = array('PublicFirstName' => 'Name',
            'BusinessName' => 'Business Name',
            'ShortDescription' => 'Short Description',
            'OrganizationDescription'=>'Organization Description',
            'building_name' => 'Building Name',
            'street_address' => 'Street Address',
            'address_details'=> 'Address Details',
            'FridayNight'=> "Will be open on friday"


        );

        // map info

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);

        if (empty($profile['map_number'])){
            $profile['map_number'] = 0;
        }

        $mapJson = $artistsListObj->listToJson(array($profile), "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');


        $socialMediaObj = new \SOSModels\ProfileSocialMedia($app['pdo']);
        $socialMediaData = $socialMediaObj->getSocailMedia($userInfo['id']);

        $mapSettings = array('bounds'=>'false', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>$profile['lat'].','.$profile['lng'] );

        // There are a bunch of sections on this page
        // + Contact Info
        // + Artist Info
        // + Artist Statement
        // + Studio Location
        // + Images

        return $app['twig']->render('for_artists/profile_home.html.twig',
            array('map_json'=>$mapJson,
                'map_settings'=> $mapSettings,

                'artist_data' => $profile,
                'image_data' => $images2,
                'thumbnail'=>$thumbnail_path,
                'atist_info_cols' => $artistInfoCols,
                'mapbook_info_cols' => $mapbokInfoCols,
                'side_menu' => $menu,
                'infoArray' => $infoArray,
                'social_data' => $socialMediaData,
                'warningArray' => $warningArray));

        //return new Response("Actor Action respose (bar)".$id);

    }


    // /update_contact_information

    public function updateContactInfo(Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();


        $profileDataToShow = array('first name' => $profile['FirstName'],
            'last name' => $profile['LastName'],
            'street address' => $profile['HomeAddressOne'],
            'street address2' => $profile['HomeAddressTwo'],
            'city' => $profile['HomeCity'],
            'state' => $profile['HomeState'],
            'zip_code' => $profile['HomeZip'],
            'phone' => $profile['Phone'],
            'email' => $profile['EmailAddress']
        );


        // get form

        $formObj = new \SOSForms\ContactInformationForm();
        $form = $formObj->getForm($app, $profile);
        $formStatus = false;

        // Check form if submitted

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                // Send user to form to paypal or check
                $formStatus = $formObj->processFormData($app['pdo'], $formData, $app, $userInfo['id']);


                // reload profile with new information (it was just updated!)

                $profile = $artistData->getArtistInfo(true);
                $form = $formObj->getForm($app, $profile);

                $profileDataToShow = array('first name' => $profile['FirstName'],
                    'last name' => $profile['LastName'],
                    'street address' => $profile['HomeAddressOne'],
                    'street address2' => $profile['HomeAddressTwo'],
                    'city' => $profile['HomeCity'],
                    'state' => $profile['HomeState'],
                    'zip_code' => $profile['HomeZip'],
                    'phone' => $profile['Phone'],
                    'email' => $profile['EmailAddress']
                );

                return $app['twig']->render('/for_artists/update_contact.html.twig', array('side_menu' => $menu, 'form_status' => $formStatus, 'form' => $form->createView(), 'profile_data' => $profileDataToShow));


            }
        }
        return $app['twig']->render('/for_artists/update_contact.html.twig', array('side_menu' => $menu, 'form' => $form->createView(), 'form_status' => $formStatus, 'profile_data' => $profileDataToShow));
    }

    // THIS IS BEING USED TO UPDATE PROFILES

    // /update_artist_info

    public function updateArtistInfo(Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];


        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');


        // Get information on logged in artist
        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();


        if (is_null($userInfo)) {
            return "User not logged in";
        }


        $profileDataToShow = array('First Name' => $profile['PublicFirstName'],
            //   'artist_statement'=>$profile['artist_statement'],
            'Last Name' => $profile['PublicLastName'],
            'Business Name' => $profile['BusinessName'],
            'Short Description' => $profile['ShortDescription'],
            'Genres' => \SOS\artistsGenreList::getGenreText($profile),
            'Organization Description' => $profile['OrganizationDescription'],
            'use images for promotion' => isset($profile['use_images_for_promotion']) ? $profile['use_images_for_promotion'] : 'N',
            'home studio' => isset($profile['home_studio']) ? $profile['home_studio'] : 'N',
            'Friday Night' => isset($profile['FridayNight']) ? $profile['FridayNight'] : 'N',
            'Fashion Info' => isset($profile['FashionInfo']) ? $profile['FashionInfo'] : 'N',
            'Artist Statement' => $profile['statement']
        );


        // get form

        $formObj = new \SOSForms\ProfileForm();
        $form = $formObj->getForm($app, $profile);

        // Check form if submitted

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $formData = $form->getData();

                $errors = 0;

                if (count($formData['genreList']) > 3) {
                    $app['session']->getFlashBag()->add('danger', 'Not Saved. You can only choose 3 genres');

                    $error = new FormError("You can only choose 3 genres");
                    $form->get('genreList')->addError($error);
                } else {

                    $dbo = $app['pdo'];
                    // Send user to form to paypal or check
                    $formStatus = $formObj->processFormData($dbo, $profile, $formData, $app, $userInfo['id']);

                    $artistObj = new \SOSModels\ArtistsGenreList($dbo);
                    $ynGenreList = $artistObj->createGenreYNArray($formData['genreList'], $formData['other'], $dbo);

                    $results2 = $artistObj->updateGenreData($ynGenreList, $profile, $userInfo['id'], $dbo);


                    // reload profile with new information
                    $profile = $artistData->getArtistInfo(true);
                    $form = $formObj->getForm($app, $profile);

                    $profileDataToShow = array('First Name' => $profile['PublicFirstName'],
                        //   'artist_statement'=>$profile['artist_statement'],
                        'Last Name' => $profile['PublicLastName'],
                        'Business Name' => $profile['BusinessName'],
                        'Short Description' => $profile['ShortDescription'],
                        'Genres' => \SOS\artistsGenreList::getGenreText($profile),
                        'Organization Description' => $profile['OrganizationDescription'],
                        'use images for promotion' => isset($profile['use_images_for_promotion']) ? $profile['use_images_for_promotion'] : 'N',
                        'home studio' => isset($profile['home_studio']) ? $profile['home_studio'] : 'N',
                        'Friday Night' => isset($profile['FridayNight']) ? $profile['FridayNight'] : 'N',
                        'Fashion Info' => isset($profile['FashionInfo']) ? $profile['FashionInfo'] : 'N',
                        'Artist Statement' => $profile['statement']

                    );

                    return $app['twig']->render('/for_artists/update_profile.html.twig', array('profile_data' => $profileDataToShow, 'side_menu' => $menu, 'form_status' => $formStatus, 'form' => $form->createView()));

                }
            }
        }
        return $app['twig']->render('/for_artists/update_profile.html.twig', array('profile_data' => $profileDataToShow, 'side_menu' => $menu, 'form' => $form->createView()));
    }


    public function updateLocation(Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();


        $locationID = $profile ['location_id'];

        if (empty($locationID)) {
            $locationID = 1;
        }
        $locationObj = new \SOSModels\Location($app['pdo']);
        $locationInfo = $locationObj->getLocation($locationID);


        // get form

        $formObj = new \SOSForms\LocationForm($app['pdo'], $locationObj);
        $form = $formObj->getForm($app, $profile, $locationInfo);
        $formStatus = false;

        // Check form if submitted

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                // Send user to form to paypal or check
                $formStatus = $formObj->processFormData($userInfo['id'], $formData, $app);


                // reload profile with new information
                $profile = $artistData->getArtistInfo(true);
                $locationID = $profile ['location_id'];

                $locationInfo = $locationObj->getLocation($locationID);

                $form = $formObj->getForm($app, $profile, $locationInfo);

                return $app['twig']->render('/for_artists/update_location.html.twig', array('side_menu' => $menu,
                    'form_status' => $formStatus,
                    'form' => $form->createView(),
                    'location_id' => $locationID,
                    'location_data' => $locationInfo));
            }
        }


        return $app['twig']->render('/for_artists/update_location.html.twig', array('side_menu' => $menu,
            'form' => $form->createView(),
            'form_status' => $formStatus,
            'location_id' => $locationID,
            'location_data' => $locationInfo
        ));
    }


    public function updateSocialMedia(Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();

        // get form

        $formObj = new \SOSForms\ProfileSocialMedia();
        $form = $formObj->getForm($app, $profile);
        $formStatus = false;

        // get existing profile information

        $member_id = $userInfo['id'];
        $socialMediaObj = new \SOSModels\ProfileSocialMedia($app['pdo']);
        $socialMediaData = $socialMediaObj->getSocailMediaWithBlanks($member_id);
        $socialMediaColumns = $socialMediaObj->getSocialMediaColumn();


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();


                // Send data to form for update.
                //$formStatus = $formObj->processFormData ( $app['pdo'], $formData,  $app, $userInfo['id']) ;

                // well in this case since...time.
                // send straight to social media object...

                $results = $formObj->processFormData($app['pdo'], $formData, $app, $member_id);


                // reload profile with new information (it was just updated!)

                $profile = $artistData->getArtistInfo(true);
                $form = $formObj->getForm($app, $profile);
                $socialMediaData = $socialMediaObj->getSocailMediaWithBlanks($member_id);


                return $app['twig']->render('/for_artists/update_social_media.html.twig', array('side_menu' => $menu,
                    'form_status' => $formStatus,
                    'form' => $form->createView(),
                    'profile_data' => $profile,
                    'social_data' => $socialMediaData,
                    'social_data_columns' => $socialMediaColumns));


            }
        }
        return $app['twig']->render('/for_artists/update_social_media.html.twig', array('side_menu' => $menu,
            'form' => $form->createView(),
            'form_status' => $formStatus,
            'profile_data' => $profile,
            'social_data' => $socialMediaData,
            'social_data_columns' => $socialMediaColumns));
    }


    public function updateImages(Request $request, Application $app)
    {

        $imageSets = array(1 => array(1, 2, 3), 4 => array(4, 5, 6, 7));

        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();
        $imageData = $artistData->getArtistImagesData();


        // get form


        // build a bunch of forms.. One per image

        $formArray = array();
        foreach ($imageData as $oneImageData) {
            $formObj = new \SOSForms\ImageForm();
            $key = $oneImageData['ImageNumber'];
            $formArray[$key] = $formObj->getForm($app, $oneImageData);

        }

        $formStatus = false;
        $uploaded = false;

        // Check form if submitted
        // loop through forms and update as needed

        foreach ($formArray as $oneForm) {

            $oneForm->handleRequest($request);
            if ($oneForm->isSubmitted()) {
                if ($oneForm->isValid()) {
                    if ($uploaded == false) {

                        $formData = $oneForm->getData();
                        $uploaded = true;

                        // Send user to form to paypal or check
                        $file = $oneForm['attachment']->getData();
                        $formStatus = $formObj->processFormData($app['pdo'], $formData, $file, $app, $userInfo['id']);

                        // reload profile with new information (it was just updated!)
                        $imageData = $artistData->getArtistImagesData();

                    }
                }
            }
        }

        // regen forms
        $formArray = array();
        foreach ($imageData as $oneImageData) {
            $formObj = new \SOSForms\ImageForm();
            $key = $oneImageData['ImageNumber'];
            $formArray[$key] = $formObj->getForm($app, $oneImageData);

        }

        foreach ($formArray as $key => $oneForm) {
            $formArray[$key] = $oneForm->createView();

        }


        return $app['twig']->render('/for_artists/update_images.html.twig', array('side_menu' => $menu,
            'form_array' => $formArray,
            'form_status' => $formStatus,
            'image_data' => $imageData,
            'profile_data' => $profile));
    }


    public function updateThumbnails(Request $request, Application $app)
    {

        $imageSets = array(1 => array(1, 2, 3), 4 => array(4, 5, 6, 7));

        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();
        $thumbData = $artistData->getArtistThumbnailData();
        $imageData = $artistData->getArtistImagesData();



        if (!empty($_POST['image']) ) {

            if ($_POST['image'] != 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAABCElEQVR4nO3BAQEAAACCIP+vbkhAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwadG3AAEtdlNaAAAAAElFTkSuQmCC') {
                $data = $_POST['image'];


                list($type, $data) = explode(';', $data);

                list(, $data) = explode(',', $data);


                //$data = base64_decode($data);

                $saveFilename = $thumbData['path'] . $userInfo['id'] . '.jpg';

                $imagej = imagecreatefrompng('data://image/png;base64,' . $data);
                imagejpeg($imagej, $saveFilename, 100);

                $app['session']->getFlashBag()->add('info', 'thumbnail updated');
                return "true";
            } else {
                $app['session']->getFlashBag()->add('danger', 'Not Saved. image is blank');
                return "false";
            }
        }
        //return "===";
        /* rraman: croppie thumbnail processing

        if (isset($_POST['croppie_image_index'])) {
            $croppieImageIndex = $_POST['croppie_image_index'];
            $croppieImagePoints = $_POST['croppie_image_points'];

            $im = imagecreatefromjpeg($sos_image_path . $id . '-' . str_pad($croppieImageIndex, 2, '0', STR_PAD_LEFT) . '.jpg');
            $points = explode(',', $croppieImagePoints);
            $points[2] = $points[2] - $points[0];
            $points[3] = $points[3] - $points[1];

            $im2 = imagecrop($im, ['x' => $points[0], 'y' => $points[1], 'width' => $points[2], 'height' => $points[3]]);
            if ($im2 !== FALSE) {
                $thumb = imagecreatetruecolor(250, 250);
                if ($thumb !== FALSE) {
                    $success = imagecopyresampled($thumb, $im2, 0, 0, 0, 0, TMAX_WIDTH, TMAX_HEIGHT, $points[2], $points[3]);
                    if ($success) {
                        $target = $thumbData['path']. $userInfo['id'].'.jpg';
                        $success = imagejpeg($thumb, $target, 90);
                    }
                    imagedestroy($thumb);
                }
                imagedestroy($im2);
            }
            imagedestroy($im);
        }

        END rraman: croppie thumbnail processing */


        return $app['twig']->render('/for_artists/update_thumbnails.html.twig', array('side_menu' => $menu,
            //'form' => $form->createView(),
            //'form_status' => $formStatus,
            'thumb_data' => $thumbData,
            'profile_data' => $userInfo));

    }


}