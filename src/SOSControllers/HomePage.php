<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/2/15
 * Time: 12:36 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class HomePage {


    public function index(Request $request, Application $app) {

        $app['request'] = $request;

        // get menu

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $randomArtists = $artistsListObj->getRandom(12);

        $thumb_dir='artist_files/artist_images/splash/';

        // check if image files exist..  Only show files that exist so collect artists that have images
        $imageCount = 0;
        $imageMax = 4;
        $artistArray = array();
        foreach ($randomArtists as $key=>$oneArtist ){
            if ($imageCount < $imageMax && file_exists($thumb_dir."/".$oneArtist['id'].".jpg" )){
                $artistArray[]= $oneArtist;
                $imageCount++;
            }
        }


        // \SOSModels\ArtistsList::resetSearchIds($app);
        // Search Form

        $searchForm = new \SOSForms\SearchArtistForm();

        $form = $searchForm->getForm($app);

        // Check form if submitted


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                $returnPage = $searchForm->processFormData($formData, $app);

                return $returnPage;
            }
        }

        $app['twig']->addGlobal('request', $request);
        $app['request'] = $request;


        $data = array(
            'name' => 'Your name',
            'email' => 'Your email',
        );


        return $app['twig']->render('index.html.twig',array("random_artists"=>$artistArray, "search_form"=>$form->createView()));


    }

}