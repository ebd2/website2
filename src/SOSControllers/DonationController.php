<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


class DonationController {



    public function donate ( Request $request, Application $app){
        $supportMenu = array('SUPPORT'=>'support.home','SPONSOR' =>'support.sponsor','VOLUNTEER'=>'volunteer.signup','DONATE'=>'support.donate');

        $app['request'] = $request;

        //echo $id ."<br>";
        //  $artistProfile = new \SOSModels\ArtistData($app['pdo'], $id);

        //$profile = $artistProfile->getArtistInfo();
        //$images =  $artistProfile->getArtistImagesData();
        //$artistData=null;

        $form = \SOSForms\DonationForm::getFrom($app);


        // Check form if submitted


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                // Send user to form to paypal or check

                $formResult = \SOSForms\DonationForm::processFromData ( $app['pdo'], $formData,  $app);
                return $app['twig']->render('/support/payment_confirm.html.twig',array('main_menu' => $supportMenu, 'formdata'=>$formData));


            }
        }
        return $app['twig']->render('/support/donate.html.twig',array('main_menu' => $supportMenu, 'form'=>$form->createView()));
    }


}