<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

use Silex\Application;
use Symfony\Component\Form\FormError;


class AdminTwentyController {



    public function showTwenty(Request $request, Application $app)
    {
        $app['request'] = $request;

        $dbo = $app['pdo'];


        // get the data into an array.

        $sql = "SELECT * FROM `twenty_fund`";
        $stmt = $dbo->prepare($sql);
        // $stmt->bindValue(':mapnum', $mapNumber, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        //var_dump ($all_rows);

        return $app['twig']->render('admin/admin_template.html.twig',array('adminData' => $all_rows));

        //return new Response("Actor Action respose (bar)".$id);







    }



}