<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


// Class to retreive artist data from the database and display
//
// Some functions show search and by medium


class ArtistsDirectoryController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }

    private function storeSearchIds (Request $request, Application $app, $artists, $searchName ='search'){

        $app['request'] = $request;

        $url = $request->getRequestUri();
        $resultSimple = array_map(function($row) {return $row['id'];}, $artists);

        /* searchType=alpha may result in duplicate rows if an artist's PublicLastName and BusinessName
           both have same initial letter.  Remove duplicates from $resultSimple so that 'Next Result'
           and 'Previous Result' iterations on artist profile pages work correctly. */

        $resultSimple = array_values(array_unique($resultSimple));

        $app['session']->set('searchList', $resultSimple);
        $app['session']->set('searchName', $searchName);
        $app['session']->set('searchURL', $url);


    }


    private function checkThumbnails ($artistData){

        $app['request'] = $request;

        // use previous thumbnail data to filter
        if (!empty ($app['session']->get('thunmbnailImagesAvailable') )){



        } else {


            $app['session']->set('thunmbnailImagesAvailable', $x);
        }

    }





    function showRandom($type, Request $request, Application $app) {
        $app['request'] = $request;

        //echo $id ."<br>";
        $listName = "Random Order";
        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $artists = $artistsListObj->getRandom(500);

        // the only view for random is grid
        $viewlist = 'artists/artist_list_grid.html.twig';

        if ($type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';
        }

        // No Map option for random. That doesn't make sence

        $mapSettings = array('bounds'=>'true', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );

        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_random', array('type'=> 'list'));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_random', array('type'=> 'grid'));


        $typeLinks = '';
        $this->storeSearchIds($request, $app, $artists, $listName);
        $mapJson=null;

        return $app['twig']->render($viewlist, array('artist_data'=>$artists, 'map_json'=> $mapJson, 'list_name'=>$listName, 'main_menu' =>  $app['artistsMenu'], 'map_settings'=> $mapSettings, 'type_link'=>$typeLinks ));



    }
        


    public function showListbySearchTerm($search, Request $request, Application $app){
        $app['request'] = $request;

        //echo $id ."<br>";

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);

        $searchName = "Arists search :".$search;

        $artists = $artistsListObj->getBySearchTerm($search);
        $this->storeSearchIds($request, $app, $artists, $searchName);

        return $app['twig']->render('artists/artist_list.html.twig',array('artist_data'=>$artists, 'list_name'=>$searchName, 'main_menu' =>  $app['artistsMenu'] ));

        //return new Response("Actor Action respose (bar)".$id);


    }

    //------------------------------------
    // BY GENRE

    public function showListbyGenre2($type, $genre,  Request $request, Application $app){

        //echo $id ."<br>";
        $app['request'] = $request;


        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $mapJson = '';

        if (array_key_exists($genre, \SOSModels\ArtistsGenreList::$column_name_to_genre_name)) {
            $genreName = 'Artists By Genre : ' . \SOSModels\ArtistsGenreList::$column_name_to_genre_name[$genre];
        } else {
            $genreName = 'Artists By Genre ';
        }

        // Map, only show participating artitst
        if ($type =='map'){
            $artistsListObj->setSort('map_number');
            $artistsListObj->setOnlyParticipating();
        }
        $artists = $artistsListObj->getByGenre($genre);
        $viewlist = 'artists/artist_list.html.twig';

        if ($type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';
        } elseif ($type =='map'){
            $viewlist = 'artists/artist_list_map.html.twig';
            $mapJson = $artistsListObj->listToJson($artists, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');

        }

        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_list_genre2', array('type'=> 'list', 'genre'=>$genre));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_list_genre2', array('type'=> 'grid', 'genre'=>$genre));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_list_genre2', array('type'=> 'map', 'genre'=>$genre));

        $mapSettings = array('bounds'=>'true', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );
        $this->storeSearchIds($request, $app, $artists, $genreName);


        return $app['twig']->render($viewlist, array('artist_data'=>$artists, 'map_json'=> $mapJson,'map_settings'=> $mapSettings, 'list_name'=>$genreName, 'main_menu' =>  $app['artistsMenu'], 'type_link'=>$typeLinks ));

    }
    //------------------------------------
    // BY SEARCH TERM


    public function showListbySearchTerm2($type, $search, Request $request, Application $app){
        $app['request'] = $request;

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);

        $searchName = "Arists search :".$search;

        $artists = $artistsListObj->getBySearchTerm($search);

        $viewlist = 'artists/artist_list.html.twig';

        if ($type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';
        }
        $this->storeSearchIds($request, $app, $artists, $searchName);
        return $app['twig']->render($viewlist, array('artist_data'=>$artists, 'list_name'=>$searchName, 'main_menu' =>  $app['artistsMenu'] ));

    }

    //------------------------------------
    // BY MAP NUMBER


    public function showListbyMap($type, $mapNumber, Request $request, Application $app){
        $app['request'] = $request;

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $artistsListObj->setSort('map_number');

        $searchName = "Map Number #".$mapNumber;

        $artists = $artistsListObj->getByMapNumber($mapNumber);

        $viewlist = 'artists/artist_list_map.html.twig';
        $type = 'map';
        $mapJson = $artistsListObj->listToJson($artists, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');
        if ($type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';
        }

        $mapSettings = array('bounds'=>'true', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );
        $this->storeSearchIds($request, $app, $artists, $searchName);

        return $app['twig']->render($viewlist, array('artist_data'=>$artists, 'map_settings'=> $mapSettings, 'map_json'=> $mapJson, 'list_name'=>$searchName, 'main_menu' =>  $app['artistsMenu'] ));

    }
    


    //------------------------------------
    // BY LETTER


    public function showListbyLetter($type, $letter, Request $request, Application $app){
        $app['request'] = $request;

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $mapJson = '';
        $listName = 'by letter : '. htmlspecialchars($letter);

        // Map, only show participating artitst
        if ($type =='map'){
            $artistsListObj->setSort('map_number');
            $artistsListObj->setOnlyParticipating();
        }
        $artists = $artistsListObj->getByLetter($letter);
        $viewlist = 'artists/artist_list.html.twig';

        if ($type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';
        } elseif ($type =='map'){
            $viewlist = 'artists/artist_list_map.html.twig';
            $mapJson = $artistsListObj->listToJson($artists, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');

        }

        $mapSettings = array('bounds'=>'true', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );

        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_list_alpha', array('type'=> 'list', 'letter'=>$letter));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_list_alpha', array('type'=> 'grid', 'letter'=>$letter));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_list_alpha', array('type'=> 'map', 'letter'=>$letter));
        $this->storeSearchIds($request, $app, $artists, $listName);

        return $app['twig']->render($viewlist, array('artist_data'=>$artists, 'map_json'=> $mapJson, 'list_name'=>$listName, 'main_menu' =>  $app['artistsMenu'], 'map_settings'=> $mapSettings, 'type_link'=>$typeLinks ));

    }

    //-------------------------------
    // show all artists.

    public function showListAll($type,   Request $request, Application $app){

        //echo $id ."<br>";
        $app['request'] = $request;


        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $mapJson = '';


        $searchName = 'All Artists ';


        // Map, only show participating artitst
        if ($type =='map'){
            $artistsListObj->setSort('map_number');
            $artistsListObj->setOnlyParticipating();
        }
        $artists = $artistsListObj->getAll();
        $viewlist = 'artists/artist_list.html.twig';

        if ($type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';
        } elseif ($type =='map'){
            $viewlist = 'artists/artist_list_map.html.twig';
            $mapJson = $artistsListObj->listToJson($artists, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"');

        }

        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_list_all', array('type'=> 'list'));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_list_all', array('type'=> 'grid'));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_list_all', array('type'=> 'map'));

        $mapSettings = array('bounds'=>'true', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );
        $this->storeSearchIds($request, $app, $artists);


        return $app['twig']->render($viewlist, array('artist_data'=>$artists, 'map_json'=> $mapJson,'map_settings'=> $mapSettings, 'list_name'=>$searchName, 'main_menu' =>  $app['artistsMenu'], 'type_link'=>$typeLinks ));

    }



}