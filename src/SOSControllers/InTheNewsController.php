<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


class InTheNewsController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }

    public function listnews(Request $request, Application $app){

        $app['request'] = $request;

        $start_num=1;
        $stmt = $app['pdo']->prepare('SELECT * FROM `news_links` ORDER BY `article_date` DESC LIMIT 0,1000');
        $stmt->bindValue(1, $start_num, \PDO::PARAM_INT);
        $stmt->execute();
        $allNews= $stmt->fetchAll( \PDO::FETCH_ASSOC);

        return $app['twig']->render('about/in_the_news.html.twig',array('news_data'=>$allNews, 'side_menu' =>  $app['aboutMenu'] ));

        //return new Response("Actor Action respose (bar)".$id);


    }

}