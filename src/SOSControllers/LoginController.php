<?php

namespace SOSControllers;
//use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


/*'sos_user_info'
    array (size=9)
      'id' => string '592' (length=3)
      'EmailAddress' => string 'acomjean2@gmail.com' (length=19)
      'epassword' => string '342b9c30e9464068c4dcf69ead0f98b1' (length=32)
      'password' => string '$2y$10$VPDxTIeDEUiODTNDafPkAOWXuxHSycTGVv9yDmiNZUMNoJFcvr9J2' (length=60)
      'Active' => string 'N' (length=1)
      'MemberType' => string '2' (length=1)
      'Expanded_Web' => string 'N' (length=1)
      'Pending' => string 'Y' (length=1)
      'Pending_Date' => string '2015-09-28 21:16:46' (length=19)
*/
class LoginController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }

    public function autologin ($uid, $hash, Request $request, Application $app)
    {
        $app['request'] = $request;



        $LoginService = new \SOS\LoginService($app['pdo'], $app);

        $loginCheck = $LoginService->checkAutoLogin($uid, $hash);


        $loginOK = $loginCheck->result;
        if ($loginOK && $loginCheck->memberRow['Active'] != "Y") {

            if ($loginCheck->memberRow['Pending'] == "Y") {
                $message = "Your membership has been received and is pending.  You should hear back from us soon";
            } else {
                $message = "Account Not Active for current year.";
            }
            $app['session']->getFlashBag()->add('danger', $message);
            $app['session']->remove('sos_user_info');
            $loginOK = false;


        } elseif ($loginOK) {
            $app['session']->getFlashBag()->add('info', 'LOGIN SUCCESSFULL ');
            $app['session']->set('sos_user_info', $loginCheck->memberRow);

            // http://stackoverflow.com/questions/29952025/silex-session-set-a-lifetime
            //$app['session']->migrate(false, 3600);
            return $app->redirect($app["url_generator"]->generate("artists.my_profile"));

        } else {
            sleep(2);
            $app['session']->getFlashBag()->add('danger', 'Could not log you in with that url.  please try again with username and password.');
            return $this->login($request, $app);
        }

        return $this->login($request, $app);

    }
        //----------------
    // LOGIN
    //----------------

    public function login( Request $request, Application $app){
        $app['request'] = $request;
        $infoArray = array();
        $warningArray = array();
        $loginOK = false;

        $form = $app['form.factory']->createBuilder(FormType::class)
            ->setAction('login')
            ->setMethod('POST')
            ->add('username', TextType::class,  array('label' => 'Username',
                'required'=> true,
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => '' ))
            ->add('password',  PasswordType::class , array('label' => 'Password',
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),

                'required'=> true))
            ->add('submit', SubmitType::class)
            ->getForm()
        ;



        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password

                $LoginService = new \SOS\LoginService($app['pdo'], $app);


                $loginCheck = $LoginService->checkLogin($formData['username'], $formData['password']);


                $loginOK = $loginCheck->result;
                if ($loginCheck->memberRow['Active']!="Y"){
                    if ($loginCheck->memberRow['Pending'] == "Y") {
                        $message = "Your membership has been received and is pending.  You should hear back from us soon";
                    } else {
                        $message = "Account Not Active for current year.";
                    }
                    $app['session']->getFlashBag()->add('danger', $message);
                    $app['session']->remove('sos_user_info');
                    $loginOK = false;


                } elseif ($loginOK) {
                    //$infoArray[] = " LOGIN SUCCESSFULL ";
                    $app['session']->getFlashBag()->add('info', 'LOGIN SUCCESSFULL ');
                    $app['session']->set('sos_user_info', $loginCheck->memberRow);

                    // http://stackoverflow.com/questions/29952025/silex-session-set-a-lifetime
                    //$app['session']->migrate(false, 3600);


                } else {
                    sleep(2);
                    $app['session']->getFlashBag()->add('danger',  'Incorrect Password, please try again');
                }

            } else {
                $app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }




        return $app['twig']->render('for_artists/login.html.twig',array( 'login_ok'=> $loginOK, 'main_menu' =>  '', 'form'  => $form->createView()));

        //return new Response("Actor Action respose (bar)".$id);

    }


    public function logout( Request $request, Application $app){

        // http://api.symfony.com/master/Symfony/Component/HttpFoundation/Session/Session.html#remove%28%29

        $userId =$app['session']->get('sos_user_info');
        $app['session']->remove('sos_user_info');

        return "logged out";

    }


    //----------------
    // RESET REQUEST
    //----------------


    public function reset_request( Request $request, Application $app){
        $app['request'] = $request;
        $infoArray = array();
        $warningArray = array();
        $resetOK = false;

        $formObj = new \SOSForms\PasswordResetForm();
        $form = $formObj->getFormReset($app);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password


                $formObj-> processResetForm($app, $formData);


            } else {
                //$warningArray[] = 'Form could not be processed, please try again';
                $app['session']->getFlashBag()->add('Form could not be processed, please try again');
                //$app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }


        return $app['twig']->render('for_artists/password_reset_request.html.twig',array( 'reset_ok'=> $resetOK, 'main_menu' =>  '', 'form'  => $form->createView()));

        //return new Response("Actor Action respose (bar)".$id);

    }
    //----------------
    // Password Update
    //----------------


    public function passwordUpdate($uid, $hash, Request $request, Application $app){
        $app['request'] = $request;
        $infoArray = array();
        $warningArray = array();
        $userIDFound = false;
        $resetOK = false;

        $LoginService = new \SOS\LoginService($app['pdo'], $app);
        // lookup user based on hash
        $userID = $LoginService->checkHashValue($hash);

        if (is_null($userID)){
            $warningArray[]="Reset Code not found..";
        } else {
            $userIDFound = true;
        }


        $form = $app['form.factory']->createBuilder(FormType::class)
            ->setAction('update_password')
            ->setMethod('POST')
            ->add('pw1', PasswordType::class,  array('label' => 'New Password', 'required'=> true, 'data' => '' ))
            ->add('hash', HiddenType::class,  array('data' => $hash))
            ->add('submit', SubmitType::class)
            ->getForm()
        ;

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password
                //var_dump ($formData);
                $userID = $LoginService->checkHashValue($formData['hash']);
                $userIDFound = true;

                $resetOK = $LoginService->setPassword($userID, $formData['pw1']);
                $resetOK = true;

                $infoArray = array();
                $warningArray = array();

            } else {
                $warningArray[] = 'Form could not be processed, please try again';
                //$app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }


        return $app['twig']->render('for_artists/password_update.html.twig',array( 'reset_ok'=> $resetOK,'user_id_found'=>$userIDFound, 'main_menu' =>  '', 'form'  => $form->createView()));

        //return new Response("Actor Action respose (bar)".$id);

    }

}