<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/3/15
 * Time: 12:56 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;



class AjaxData {

    private $sos_dbo;





    public function completeStreetLocation($string,  Request $request, Application $app){

        $locationObj = new \SOSModels\Location($app['pdo']);
        $result = $locationObj->completeStreetName($string);

        $resultArray = [];
        foreach ($result as $oneResult){
            $resultArray[]= $oneResult['street_name'];
        }


        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $response->setEncodingOptions(JSON_NUMERIC_CHECK);
        $response->setData($resultArray);

        return $response;



    }


}