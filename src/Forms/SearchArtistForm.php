<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class SearchArtistForm {



    public  function getForm(\Silex\Application $app) {


        $defaultData = null;

        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)
            ->setMethod('GET')
            ->add('search_term', TextType::class, array('label'=>'Name :','required' => false))
            ->add('submit',  SubmitType::class, [
                'label' => 'submit'])
            ->getForm();

        return $form;

    }

    // return a redirect page for the search form.
    // redirect page will have search results.

    public  function processFormData ($formData, Application $app ){

        $searchTerm = trim($formData['search_term']);


        // redirect to all
        if (empty($searchTerm)){
            $app['session']->getFlashBag()->add('warning',  'No search term entered, returning all artists');

            return $app->redirect($app["url_generator"]->generate("artists_directory_list_all",  array('type' => 'list')));
        }

        return $app->redirect($app["url_generator"]->generate("artists_directory_search",  array('search' => $searchTerm)));



    }
}