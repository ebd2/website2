<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class ProfileFormImages {

    private static $sql_debug = false;
    private static $debug = false;


    public  function getForm(\Silex\Application $app, $profile) {


        $defaultData = array('first_name'=> $profile['PublicFirstName'],
            'last_name'=> $profile['PublicLastName'],
            'business_name'=> $profile['BusinessName'],
            'artist_short_description'=> $profile['ShortDescription'],
            'organization_description'=> $profile['OrganizationDescription'],
            'state'=> $profile['HomeState'],
            'zip_code'=> $profile['HomeZip'],
            'phone'=> $profile['Phone'],
            'email'=> $profile['EmailAddress']
            );


        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)
            ->add('first_name', TextType::class, array('label'=>'First Name',
               'required'   => false
            ))
            ->add('last_name', TextType::class, array('label'=>'Last Name/ Business Name',
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('artist_short_description', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max'=>'25')))
            ));

            if ($profile['BusinessNameOption']=='Y'){
                $form = $form->add(

            }

            $form = $form->add('artist_statement', TextareaType::class, array(
                'required'   => false
            ))
            ->add('website', TextType::class, array( 'required'   => false,
                'label' => 'webiste url (http://www....)',
            ))

            ->add('submit', SubmitType::class, [
                'label' => 'Submit/Update',
            ])
            ->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, $formData, Application $app, $memberid ){

        $year = "";

        $sql = sprintf("update member set FirstName= :FirstName, LastName= :LastName, EmailAddress= :EmailAddress, HomeAddressOne= :AddressOne, HomeAddressTwo= :AddressTwo, HomeCity= :City, HomeState= :State, HomeZip= :Zip, Phone= :Phone where id= :id LIMIT 1");

        $stmt = $dbo->prepare ($sql);

        $stmt->bindParam(':FirstName', $formData['first_name'] , \PDO::PARAM_STR);
        $stmt->bindParam(':LastName', $formData['last_name'] , \PDO::PARAM_STR);
        $stmt->bindParam(':EmailAddress', $formData['email'], \PDO::PARAM_STR);
        $stmt->bindParam(':AddressOne', $formData['street_address'] , \PDO::PARAM_STR);
        $stmt->bindParam(':AddressTwo', $formData['street_address2'] , \PDO::PARAM_STR);
        $stmt->bindParam(':City', $formData['city'] , \PDO::PARAM_STR);
        $stmt->bindParam(':State', $formData['state']  , \PDO::PARAM_STR);
        $stmt->bindParam(':Zip', $formData['zip_code']  , \PDO::PARAM_STR);
        $stmt->bindParam(':Phone', $formData['phone'] , \PDO::PARAM_STR);
        $stmt->bindParam(':id', $memberid , \PDO::PARAM_INT);

        $resultData = true;
        if ($stmt->execute()) {

            $app['session']->getFlashBag()->add('info','Form updated successfully');

        } else {

            $app['session']->getFlashBag()->add('danger','Contact information could not be processed, please try again');
            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            $resultData = false;
        }


        return $resultData;


    }

}