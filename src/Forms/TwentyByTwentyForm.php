<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class TwentyByTwentyForm {

    private static $sql_debug = false;
    private static $debug = false;


    public static function getFrom(\Silex\Application $app) {
        $states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        );

        $form = $app['form.factory']->createBuilder(FormType::class)
            ->add('first_name', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('last_name', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('email', TextType::class, array(
                'label' => 'Email Address',
                'constraints' => new Assert\Email()
            ))
            ->add('phone', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))

            ->add('comments', TextType::class, array(  'required'   => false
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Sign Up',
            ])
            ->getForm();


        return $form;

    }

    public static function processFromData ( \PDO $dbo, $formData, Application $app){

        $year = "";


        $sql = 'INSERT INTO `twenty_fund` (`id`, `contact_fname`,  `phone`, `amount`, `year`,  `contact_email`, `date`, `contact_lname`, `comments`) VALUES (NULL, :first_name,  :phone, :amount, :year, :email , :date, :last_name, :comments);';


        $mysql_date=  date("Y-m-d H:i:s");
        $year = date("Y");
        $amount = 20;

        $stmt = $dbo->prepare($sql);

        $stmt->bindValue(':first_name',$formData['first_name'], \PDO::PARAM_STR);
        $stmt->bindValue(':last_name',$formData['last_name'], \PDO::PARAM_STR);
        $stmt->bindValue(':amount',$amount, \PDO::PARAM_STR);

        $stmt->bindValue(':phone',$formData['phone'], \PDO::PARAM_STR);
        $stmt->bindValue(':year',$year, \PDO::PARAM_STR);
        $stmt->bindValue(':email',$formData['email'], \PDO::PARAM_STR);
        $stmt->bindValue(':comments',$formData['comments'], \PDO::PARAM_STR);

        $stmt->bindValue(':date',$mysql_date, \PDO::PARAM_STR);


        if ($stmt->execute()) {

            $resultData = $stmt->fetchAll(\PDO::FETCH_ASSOC | \PDO::FETCH_GROUP);

        } else {


            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            $resultData = null;
        }


        return $resultData;


    }

}