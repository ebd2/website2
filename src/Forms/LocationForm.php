<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class LocationForm {

    private $dbo;
    private $locationObj;


    public function __construct( \PDO $dbo, $locationObj)
    {
        $this->dbo = $dbo;
        $this->locationObj = $locationObj;

    }


    public function getForm(\Silex\Application $app, $profile, $location)
    {

        // we're getting the location field because it contains more details than are available in the profile



        $defaultData = array('street_number'=> $location['street_number'],
            'street_name' => $location['street_name'],
            'address_details' => $profile['address_details'],
            'home_studio' => $profile['home_studio'],
            'accessible' => $profile['HandicapAccessible']
            );
        $studioBuildings = $this->locationObj->getLocationsWith4More();
        $locationID = $location['location_id'];
        $studioDropdown = array('Other'=>'Other');
        $defaultDropdown = 'Other';

        foreach ($studioBuildings as $location_id=>$oneBuilding){
            $key = $oneBuilding['building_name']. ' - '.$oneBuilding['street_address'];
            $studioDropdown[$key] = $location_id;

            if ($locationID == $location_id){

                $defaultDropdown = $location_id;
            }

        }

        $defaultData['studio_picker']= $defaultDropdown;




        //
        //$dropdownValue =

        $nyChoice = \SOSForms\CommonForm::$nyChoice;

        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)



            ->add('studio_picker', ChoiceType::class, array( 'required'   => true,
                'choices' => $studioDropdown,
                'expanded' => false,
                'multiple'=> false,
                'label' => 'If you are in a studio building, please pick it from the list.'
            ))
            ->add('building_name', TextType::class,
                array('label'=>'Building Name (if location has more than 4 artists)',
                    'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                    'required'   => false
                ))
            ->add('street_number', TextType::class,
                array('label'=>'Street Number',
                    'attr' => array('style' => 'width:150px', 'placeholder' => ''),
                    'required'   => false
            ))
            ->add('street_name', TextType::class, array('label'=>'Street Name',
                'required' => false
            ))

            ->add('address_details', TextType::class, array(
                'label' => 'My studio # / Unit # )',
                'required'   => false
            ))
            ->add('home_studio', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'My studio is my home'
            ))
            ->add('accessible', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'My studio is accessible (Americans Disabilities Act  description)'
            ))

            ->add('submit', SubmitType::class, [
                'label' => 'Change',
            ])
            ->getForm();


        return $form;

    }


    public  function processFormData ($memberid, $formData, Application $app ){

        $resultData = $this->locationObj->saveMemberLocation ($memberid, $formData);


        return $resultData;


    }

}