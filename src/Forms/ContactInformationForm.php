<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class ContactInformationForm {

    private static $sql_debug = false;
    private static $debug = false;


    public  function getForm(\Silex\Application $app, $profile) {
        $states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        );

        $defaultData = array('first_name'=> $profile['FirstName'],
            'last_name'=> $profile['LastName'],
            'street_address'=> $profile['HomeAddressOne'],
            'street_address2'=> $profile['HomeAddressTwo'],
            'city'=> $profile['HomeCity'],
            'state'=> $profile['HomeState'],
            'zip_code'=> $profile['HomeZip'],
            'phone'=> $profile['Phone'],
            'email'=> $profile['EmailAddress']
            );



        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)
            ->add('first_name', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('last_name', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('email', TextType::class, array(
                'label' => 'Email Address',
                'constraints' => new Assert\Email()
            ))
            ->add('phone', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('street_address', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('street_address2', TextType::class, array( 'required'   => false,
                'label' => 'Unit or Apt #',
            ))

            ->add('city', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('state', ChoiceType::class, array(
                'choices' => $states,
                'expanded' => false,
                'data' => 'Massachusetts',
                'label' => 'State'
            ))
            ->add('zip_code', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Change',
            ])
            ->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, $formData, Application $app, $memberid ){

        $year = "";

        $sql = sprintf("update member set FirstName= :FirstName, LastName= :LastName, EmailAddress= :EmailAddress, HomeAddressOne= :AddressOne, HomeAddressTwo= :AddressTwo, HomeCity= :City, HomeState= :State, HomeZip= :Zip, Phone= :Phone where id= :id LIMIT 1");

        $stmt = $dbo->prepare ($sql);

        $stmt->bindParam(':FirstName', $formData['first_name'] , \PDO::PARAM_STR);
        $stmt->bindParam(':LastName', $formData['last_name'] , \PDO::PARAM_STR);
        $stmt->bindParam(':EmailAddress', $formData['email'], \PDO::PARAM_STR);
        $stmt->bindParam(':AddressOne', $formData['street_address'] , \PDO::PARAM_STR);
        $stmt->bindParam(':AddressTwo', $formData['street_address2'] , \PDO::PARAM_STR);
        $stmt->bindParam(':City', $formData['city'] , \PDO::PARAM_STR);
        $stmt->bindParam(':State', $formData['state']  , \PDO::PARAM_STR);
        $stmt->bindParam(':Zip', $formData['zip_code']  , \PDO::PARAM_STR);
        $stmt->bindParam(':Phone', $formData['phone'] , \PDO::PARAM_STR);
        $stmt->bindParam(':id', $memberid , \PDO::PARAM_INT);

        $resultData = true;
        if ($stmt->execute()) {

            $app['session']->getFlashBag()->add('info','Form updated successfully');

        } else {

            $app['session']->getFlashBag()->add('danger','Contact information could not be processed, please try again');
            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            $resultData = false;
        }


        return $resultData;


    }

}