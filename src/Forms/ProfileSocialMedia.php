<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class ProfileSocialMedia {

    private static $sql_debug = false;
    private static $debug = false;





    public function getDefaultData ($app, $profile)
    {

        $memberID = $profile['member_id'];
        $dbo = $app['pdo'];

        $socialMediaObj = new \SOSModels\ProfileSocialMedia($dbo);
        $defaultData = $socialMediaObj->getSocailMediaWithBlanks($memberID);


        return $defaultData;

    }




    public  function getForm(\Silex\Application $app, $profile)
    {

        $socialMediaFields = $this->getDefaultData($app, $profile);


        $form = $app['form.factory']->createBuilder(FormType::class);

        foreach ($socialMediaFields as $oneSocialMedia){

            $name = $oneSocialMedia['short_name'];
            $value = $oneSocialMedia['user_name'];
            $placeholder = $oneSocialMedia['description'];


            $form->add($name, TextType::class, array('label'=> $name,
                'required'   => false,
                'attr' => array('style' => 'width:380px;', 'placeholder' => $placeholder),
                'data'=> $value
            ));

        }


          $form=   $form->add('submit', SubmitType::class, [
                    'label' => 'Change',
                ])



                ->getForm();

        return $form;

    }

    public  function processFormData ( \PDO $dbo, $formData, Application $app, $member_id )
    {

        $dbo = $app['pdo'];
        $socialMediaObj = new \SOSModels\ProfileSocialMedia($dbo);
        $results = $socialMediaObj->updateFromArray($formData, $member_id);

        if ($results['status'] == "failed"){

            $app['session']->getFlashBag()->add('danger','Trouble saving all data, please try again');


        } else {
            $app['session']->getFlashBag()->add('info','Data updated.');


        }

        //foreach ($results['messages'] as $one_message){
       //     $app['session']->getFlashBag()->add('info',$one_message);
       // }


        return true;


    }

}