<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class ProfileForm {

    private static $sql_debug = false;
    private static $debug = false;




    public static function getDefaultData ($profile){

        $defaultData = array('PublicFirstName'=> $profile['PublicFirstName'],
            //   'artist_statement'=>$profile['artist_statement'],
            'PublicLastName'=> $profile['PublicLastName'],
            'BusinessName'=> $profile['BusinessName'],
            'ShortDescription'=> $profile['ShortDescription'],
            'OrganizationDescription'=> $profile['OrganizationDescription'],
            'use_images_for_promotion'=> isset($profile['use_images_for_promotion']) ? $profile['use_images_for_promotion'] :'N',
            'home_studio'=> isset($profile['home_studio']) ? $profile['home_studio'] :'N',
            'FridayNight'=> isset($profile['FridayNight']) ? $profile['FridayNight'] :'N',
            'FashionInfo'=> isset($profile['FashionInfo']) ? $profile['FashionInfo'] :'N',
            'statement'=>  $profile['statement']
        );


        return $defaultData;

    }




    public  function getForm(\Silex\Application $app, $profile) {


        $defaultData = self::getDefaultData($profile);


        $genres = array_flip(\SOSModels\ArtistsGenreList::$column_name_to_genre_name);
        $artistGenreObj =  new \SOSModels\ArtistsGenreList($app['pdo']);
        $checkedGenre = $artistGenreObj->getGenreFormCheckboxes($profile);

        /*
        print "<h3> Genre Checked </h3><pre>";

        var_dump ($checkedGenre);

        print "</pre>";

        print "<h3> Profile </h3><pre>";
        var_dump ($profile);

        print "</pre>";

        */
        $nyChoice = \SOSForms\CommonForm::$nyChoice;

        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)
            ->add('PublicFirstName', TextType::class, array('label'=>'First Name',
               'required'   => false
            ))
            ->add('PublicLastName', TextType::class, array('label'=>'Last Name/ Business Name',
                'required'   => false,
                'constraints' => array(new Assert\NotBlank(),
                    new Assert\Length(array('min' => 2)))
            ))
            ->add('ShortDescription', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max'=>'30')))
            ));

            if ($profile['BusinessNameOption']=='Y'){
                $form = $form->add('BusinessName', TextType::class, array('label'=>'Business Name',
                    'required'   => false,
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
                ));
            }


            if ($profile['MemberType'] >3){

            $form = $form->add('OrganizationDescription', TextType::class, array('label'=>'Organization Description (250 chars)',
                    'required'   => false,
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2,'max'=>250)))
                ));
            }
            
            
            
            $form = $form->add('statement', TextareaType::class, array('label'=>'Artist Statement',
                'required'   => false,
                'attr' => array('style' => 'height:180px'),
                'constraints' => array(new Assert\NotBlank(),
                    new Assert\Length(array('min' => 2, 'max'=>9000)))
            ))


            ->add('genreList', ChoiceType::class, array(

                    'choices' => $genres,
                    'expanded' => true,
                    'multiple'=> true,
                    'data' => $checkedGenre,
                    'label' => "Genre's (select up to 3)"
                ))
             /*   ->add('artist_statement', TextareaType::class, array(
                    'required'   => false,
                    'data' => $profile["statement"]
                ))
             */
                ->add('other', TextType::class, array( 'required'   => false,
                    'label' => 'other (really only if none other fit)',
                    'data' => $profile["Other"]
                ))

                ->add('use_images_for_promotion', ChoiceType::class, array( 'required'   => true,
                    'choices' => $nyChoice,
                    'expanded' => true,
                    'multiple'=> false,
                    'label' => 'Allow SOS to use images for promotion',
                    'data' => $profile["use_images_for_promotion"]
                ))
                ->add('home_studio', ChoiceType::class, array( 'required'   => true,
                    'choices' => $nyChoice,
                    'expanded' => true,
                    'multiple'=> false,
                    'label' => 'My Studio is in my home',
                    'data' => $profile["home_studio"]
                ))


                ->add('FashionInfo', ChoiceType::class, array( 'required'   => true,
                    'choices' => $nyChoice,
                    'expanded' => true,
                    'multiple'=> false,
                    'label' => 'I would like information about the fashion show'
                ))

                ->add('FridayNight', ChoiceType::class, array( 'required'   => true,
                    'choices' => $nyChoice,
                    'expanded' => true,
                    'multiple'=> false,
                    'label' => 'I will be open friday night'

                ))
                ->add('submit', SubmitType::class, [
                    'label' => 'Update',
                ])



                ->getForm();

/*
        ->add('FashionInfo', ChoiceType::class, array( 'required'   => true,
            'choices' => $nyChoice,
            'expanded' => true,
            'multiple'=> false,
            'label' => 'I would like information about the fashion show'
        ))

            ->add('FridayNight', ChoiceType::class, array( 'required'   => false,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'I will be open friday night'

            ))
  */
        return $form;

    }

    public  function processFormData ( \PDO $dbo, $originalArtistsData, $formData, Application $app, $memberid ){

        if (count($formData['genreList']) >3){
            $app['session']->getFlashBag()->add('danger','only 3 Values allowed for each genre.');
        } else {




        }

        $profileUpdateObj = new \SOSModels\ProfileUpdate($dbo);

        $status = $profileUpdateObj->updateFromArray($originalArtistsData, $formData, $memberid);

        return $status;


    }

}