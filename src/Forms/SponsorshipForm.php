<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class SponsorshipForm {

    private static $sql_debug = false;
    private static $debug = false;


    public  function getForm(\Silex\Application $app, \SOS\SponsorshipData  $sponsorObj) {

        $sponsorData = $sponsorObj->getRateData();

        // Go through the sponsor data and create a types mapping

        foreach ($sponsorData as $oneSponsor){


        }

        $states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        );


        $rateData = $sponsorObj->getRateData();

        $sponsorForSale = array();

        foreach ($rateData as $sname=>$oneSponsorship){
            if (isset($oneSponsorship['available']) &&  $oneSponsorship['available'] )  {

                $key = $oneSponsorship['name'] .' - '.$oneSponsorship['cost_text'];
                $sponsorForSale[$key] = $sname;
                }
        }



       /* $defaultData = array('first_name'=> $profile['FirstName'],
            'last_name'=> $profile['LastName'],
            'street_address'=> $profile['HomeAddressOne'],
            'street_address2'=> $profile['HomeAddressTwo'],
            'city'=> $profile['HomeCity'],
            'state'=> $profile['HomeState'],
            'zip_code'=> $profile['HomeZip'],
            'phone'=> $profile['Phone'],
            'email'=> $profile['EmailAddress']
            );
*/
        $defaultData = array();

        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)
            ->add('business_name', TextType::class, array(
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),

                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('sponsorship_type', ChoiceType::class, array(
                'choices' => $sponsorForSale,
                'expanded' =>true
            ))
            ->add('first_name', TextType::class, array(
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('last_name', TextType::class, array(
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('email', TextType::class, array(
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'label' => 'Email Address',
                'constraints' => new Assert\Email()
            ))
            ->add('phone', TextType::class, array(
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('street_address', TextType::class, array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('street_address2', TextType::class, array( 'required'   => false,
                'label' => 'Unit or Apt #',
                'attr' => array('style' => 'width:150px', 'placeholder' => ''),
            ))

            ->add('city', TextType::class, array(
                'attr' => array('style' => 'width:250px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))

            ))
            ->add('state', ChoiceType::class, array(
                'choices' => $states,
                'expanded' => false,
                'data' => 'Massachusetts',
                'label' => 'State',
                            'attr' => array('style' => 'width:150px', 'placeholder' => ''),
            ))
            ->add('zip_code', TextType::class, array(
                'attr' => array('style' => 'width:150px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))

        ))
            ->add('submit', SubmitType::class, [
                'label' => 'Click To Continue',
            ])
            ->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, $formData, Application $app, \SOS\SponsorshipData  $sponsorObj){

        $year = "";

        //$sql = sprintf("update ads set FirstName= :FirstName, LastName= :LastName, EmailAddress= :EmailAddress, HomeAddressOne= :AddressOne, HomeAddressTwo= :AddressTwo, HomeCity= :City, HomeState= :State, HomeZip= :Zip, Phone= :Phone where id= :id LIMIT 1");


        $sql ='INSERT INTO `ads` ( `business_name`, `street`, `state`, `zip`, `phone`, `type`, `year`, `city`, `contact_email`, `contact_fname`, `contact_lname`, `date`) VALUES (:business_name, :street, :state, :zip, :phone, :type, :year, :city, :contact_email, :contact_fname, :contact_lname, :date )';


        // sql_name(form name) => :name
        $paramsToBind = array('business_name' =>'business_name',
            'street_address' =>'street',
            'state' =>'state',
            'zip_code' =>'zip',
            'phone' =>'phone',
            'sponsorship_type' =>'type',
            'year'=> 'year',
            'city' => 'city',
            'email'=>'contact_email',
            'first_name'=>'contact_fname',
            'last_name'=>'contact_lname',
            'date'=>'date'
            );

        $formData['date'] = date("Y-m-d H:i:s");
        $formData['year'] = date('Y');
        $formData['type'] = "trolley";

        $stmt = \SOS\SQLHelper::bindSQL($dbo, $sql, $paramsToBind, $formData);


        if ($stmt->execute()) {
            $resultData = true;

            $app['session']->getFlashBag()->add('info','Info saved.');


        } else {

            $app['session']->getFlashBag()->add('danger','Contact information could not be processed, please try again or contact sponsorship@somervilleopenstudios.org');
            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            $resultData = false;
        }

        // send email

        $emailObj = new \SOS\EmailService();

        $message = <<<EOF

Sponsor info:  
business: {$formData['business_name']}
phone: {$formData['phone']}
email: {$formData['email']}
sponsor type: {$formData['sponsorship_type']}
address: {$formData['street_address']}
name: {$formData['first_name']}  {$formData['last_name']}
date: {$formData['date']} 


EOF;
        $emailObj->sendEmail("acomjean@gmail.com,sponsorship@somervilleopenstudios.org",
            "Sponsorship info", $message, 'webmaster@somervilleopenstudios.org' );

        $rateData = $sponsorObj->getRateData();


        // lets create a payment... Then forward to page.
        $typePurchased = $formData['sponsorship_type'];
        $cost = $rateData[$typePurchased]['cost'];

        $paymentObj = new \SOSModels\Payments($dbo);
        $name = $formData['first_name'].' '. $formData['last_name'];
        $email = $formData['email'];


        $blankPayment = $paymentObj->get_blank_payment();
        $blankPayment['item_description']= "SOS sponsorship -". $typePurchased;
        $blankPayment['payer_name']= $name;
        $blankPayment['transaction_amount'] = $cost;
        $blankPayment['donation_amount'] = 0;
        $blankPayment['payer_email']=$email;
        $blankPayment['member_id']=0;
        $blankPayment['type']='sponsorship';


        $paymentID = $paymentObj->add_payment($blankPayment);

        $hash = $paymentObj->getPaymentHash($paymentID);



        return array('pid'=>$paymentID, 'hid'=>$hash);

    }

}