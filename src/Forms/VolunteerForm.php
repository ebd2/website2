<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;




class VolunteerForm {

    public function getForm(\Silex\Application $app) {

        $volunteerOptions = $this->positionList();


        $volunteerChoices = array();
        $volunteerOptions = $this->positionList();


        foreach ($volunteerOptions as $oneVol){
            $key= $oneVol->name;
            $volunteerChoices[$key]=$oneVol->name;
        }


        $form = $app['form.factory']->createBuilder(FormType::class)
     ->setMethod('POST')
     ->add('name', TextType::class, array('label'=>'Name :','required' => true,
                     'attr' => array('style' => 'width:350px', 'placeholder' => '')

     ))
     ->add('email', TextType::class, array('label'=>'Email :',
         'required' => true,
                         'attr' => array('style' => 'width:350px', 'placeholder' => ''),

         'constraints' => new Assert\Email()))
     ->add ('vol_pos', ChoiceType::class,  array( 'label'=>"Volunteer Positions:",
         'choices'  => $volunteerChoices,
         'multiple' => true,
         'expanded' => true,
         'required' => true,

         'attr' => array('style' => 'margin-left:20px; color:blue')))
     ->add('comments', TextareaType::class, array('label'=>'','required' => false,  'attr' => array('style' => 'width:80%')))
     ->add('submit', SubmitType::class)
     ->getForm();




        return $form;
}


    /**
     *
     * Store form data.  Goes into 2 database tables (volunteer_contact, volunteer_match)
     *
     * @param $dbo
     * @param $formData
     * @return bool
     */



    public function addNewVolunteer($dbo, $formData){

        $volunteerOptions = $this->positionList();
        $volunteerChoices = array();
        foreach ($volunteerOptions as $key=>$oneVol){
            $positionName = $oneVol->name;
            if (in_array($positionName,($formData['vol_pos']))) {
                $volunteerChoices[] = $oneVol->option_name;
            }
        }


        // 1 Add new User to volunteer contact

        $sql = "INSERT INTO `volunteer_contact` (`volunteer_id`, `name`, `email_address`, `hash`, `signup_date`, `confirmed`, `comment`) VALUES (NULL, :name, :email,  :hash, :date, 'Y', :comment );";

        $stmt = $dbo->prepare ($sql);

        //set reset key to date
        //    print "$mysql_date $new_pw $local_id ";
        $mysqlDate=  date("Y-m-d H:i:s");
        $hash = sha1("sos346283^".$formData['email']."3dowexdxglkdn43d4".$mysqlDate );

        $stmt->bindParam(':name',  $formData['name'] , \PDO::PARAM_STR);
        $stmt->bindParam(':email', $formData['email'] , \PDO::PARAM_STR);
        $stmt->bindParam(':hash', $hash , \PDO::PARAM_STR);
        $stmt->bindParam(':date', $mysqlDate , \PDO::PARAM_STR);
        $stmt->bindParam(':comment',  $formData['comments'] , \PDO::PARAM_STR);
        $didWork = $stmt->execute();

        if (!$didWork){
            return false;
        }
        $lastID = $dbo->lastInsertId();

        // 2 Add to table volunteer match (each volunteer position selected)

        foreach ($volunteerChoices as $onePosition) {

            $sql = "INSERT INTO `volunteer_match` (`contact_id`, `position_name`, `position_year`) VALUES (:contact_id, :name, '2019' );";

            $stmt = $dbo->prepare($sql);

            $stmt->bindParam(':contact_id', $lastID, \PDO::PARAM_INT);
            $stmt->bindParam(':name', $onePosition, \PDO::PARAM_STR);

            $didWork = $stmt->execute();
        }


        return $didWork;

    }






        //-----------------------------
// List of Volunteer Positions
//-----------------------------
//
    public function positionList() {


        $volunteerOptions = array();

        $obj = new \stdClass;
        $obj->name = 'Sponsorship / Ad sales';
        $obj->option_name = 'sponsorship';
        $obj->description = ' - Now through end of Feb.<br>
Promote SOS sponsorship to businesses<br>
';
        $obj->number_position = 6;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Marketing';
        $obj->option_name = 'marketing';
        $obj->description = ' - Now through May.<br>
Promote and Market SOS<br>
';
        $obj->number_position = 6;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Poster Distribution';
        $obj->option_name = 'poster_dist';
        $obj->description = ' - mid-March to Mid-April. <br>
tape posters in shop windows<br>
';
        $obj->number_position = 25;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Mapbook Distribution';
        $obj->option_name = 'mapbook_dist';
        $obj->description = ' - mid-April through SOS weekend<br>
fill selected mapstands, place maps in select businesses. <br>
';
        $obj->number_position = 25;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Proof Readers';
        $obj->option_name = 'proof_readers';
        $obj->description = ' - mid March<br>
carefully read through entire mapbook<br>
';
        $obj->number_position = 3;
        $volunteerOptions[] = $obj;


//5
        $obj = new \stdClass;
        $obj->name = 'Exhibit Support - Volunteer Show';
        $obj->option_name = 'volunteer_show_exhibit_support';
        $obj->description = '<br>
 Drop-off help<br>
and help hang group art show at Bloc 11.  Dates TBD<br>
 ';
        $obj->number_position = 6;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Exhibit Support -  First Look Show ';
        $obj->option_name = 'artists_choice_exhibit_support';
        $obj->description = ' - dates in late March through mid-May<br>
accept art, process forms for drop off<br>
return art, process forms for pick up<br>
';;
        $obj->number_position = 8;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Hang First Look Show ';
        $obj->option_name = 'hang_artists_choice_show';
        $obj->description = ' - help hang show in the exhibit space<br>
';
        $obj->number_position = 10;
        $volunteerOptions[] = $obj;


//7---------------------------------------------

        $obj = new \stdClass;
        $obj->name = 'First Look Show Reception';
        $obj->option_name = 'artists_choice_reception';
        $obj->description = ' - Before or After the show help <br>
set-up food, tables.  After help clean up, put away tables<br>
';
        $obj->number_position = 5;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Map Stands Install / Remove';
        $obj->option_name = 'map_stands';
        $obj->description = ' - 1 or 2 days, early Apr.<br>
driving, lifting, installing mapstands<br>
';
        $obj->number_position = 3;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Flatbread fundraiser table sitting';
        $obj->option_name = 'flatbread';
        $obj->description = ' - Date TBD<br>
hour-long shifts: sell raffle tickets, promote SOS <br>
';
        $obj->number_position = 4;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name='Beyond the Pattern runway show';
        $obj->option_name='fasion_crew';
        $obj->description=' - Wed. May 1 2019, Thur. May 2 2019 afternoon-evening<br>
Models, lights, slides, setup, hair & makeup, greeter, ushers, photo & video, runners<br>
        (See <a href="/for_artists/fashion_show">this page</a> for show description)';
       
        $obj->number_position=8;
        $volunteerOptions[] = $obj;
        

// 9
        $obj = new \stdClass;
        $obj->name = 'During-Event Ambassadors';
        $obj->option_name = 'during_event_ambassadors';
        $obj->description = ' - Sat. May 4 2019, Sun., May 5 2019<br>
2 Hour Shifts from 11:00am - 6pm<br>
at Info Booths and on Trolleys<br>
greet visitors, explain event<br>
<i>cannot be participating artist!!</i></br>
';
        $obj->number_position = 20;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Other';
        $obj->option_name = 'other';
        $obj->description = '<br/>Fill in details of what you are looking to help with in the comment block.  This actually helps our volunteer coordinators a bit, we will read it and try and contact you as new oppurtunitied arrive';
        $obj->number_position = 'Many';
        $volunteerOptions[] = $obj;

        return $volunteerOptions;

    }

}