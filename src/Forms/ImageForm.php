<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class ImageForm {

    private static $sql_debug = false;
    private static $debug = false;


    public function getForm(\Silex\Application $app, $imageData) {
/*
        'id' => string '10923' (length=5)
      'member_id' => string '592' (length=3)
      'ImageNumber' => string '3' (length=1)
      'Title' => string 'No bon pain' (length=11)
      'Medium' => string 'Photo' (length=5)
      'Dimensions' => string 'Venti' (length=5)
      'filename' => string 'artist_files/artist_images/profile/592-03.jpg' (length=45)
*/

        $form = $app['form.factory']->createBuilder(FormType::class)

            ->add('attachment', FileType::class, array( 'required'   => false,
            'label' => 'Image (jpg or png)'
            ))
            ->add('ImageNumber', HiddenType::class, array( 'required'   => false,
                'label' => 'other (really only if none other fit)',
                'data' => $imageData["ImageNumber"]
            ))

            ->add('Title', TextType::class, array( 'required'   => false,
                'label' => 'Title',
                'data' => $imageData["Title"]
            ))
            ->add('Medium', TextType::class, array( 'required'   => false,
                'label' => 'Medium',
                'data' => $imageData["Medium"]
            ))

            ->add('Dimensions', TextType::class, array( 'required'   => false,
                'label' => 'Dimensions: eg (10"x12)',
                'data' => $imageData["Dimensions"]
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Upload/Update',
            ])
            ->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, $formData, $file, Application $app,  $memberID ){

        $target_dir = "artist_files/artist_images/profile/";

        $MAX_WIDTH  =700;
        $MAX_HEIGHT =700;
        //define('MAX_TWIDTH', 200  );
        //define('MAX_THEIGHT', 200);

        $year = "";
        /*
        'attachment' =>
    object(Symfony\Component\HttpFoundation\File\UploadedFile)[960]
      private 'test' => boolean false
      private 'originalName' => string 'Screenshot from 2018-09-24 21-24-35.png' (length=39)
      private 'mimeType' => string 'image/png' (length=9)
      private 'size' => int 257074
      private 'error' => int 0
      private 'pathName' (SplFileInfo) => string '/tmp/phpwUV14M' (length=14)
      private 'fileName' (SplFileInfo) => string 'phpwUV14M' (length=9)
*/
        //https://github.com/symfony/symfony/blob/4.2/src/Symfony/Component/HttpFoundation/File/UploadedFile.php
        //$file->move($directory, $someNewFilename);


        if (!empty($file)) {
            try {
                $extension = $file->getClientOriginalExtension();
                $mime = $file->getClientMimeType();
                $path = $file->getPathName();
                //$name= $file->getName();


                //$valid_extensions = array('jpg', 'png', 'jpeg');
                $image_num = $formData['ImageNumber'];
                $jpg_number = array('space', '-01.jpg', '-02.jpg', '-03.jpg', '-04.jpg', '-05.jpg', '-06.jpg','-07.jpg', '-08.jpg', '-09.jpg');


                $name = $memberID . $jpg_number[$image_num];
                $target_file = $target_dir . $name;

                $image = new \SOS\SimpleImage();
                $loadStatus = $image->load($path);

                if (!$loadStatus) {
                    $app['session']->getFlashBag()->add('danger','Unable to update '.$formData['ImageNumber']  .' Image - '. $image->message);
                }else{
                    $w = $image->getWidth();
                    $h = $image->getHeight();

                    if ($w > $h) {
                        $image->resizeToWidth($MAX_WIDTH);
                    } else {
                        $image->resizeToHeight($MAX_HEIGHT);
                    }
                    $image->save($target_file);
                    $app['session']->getFlashBag()->add('info', 'Image ' . $formData['ImageNumber'] . ' updated successfully');
                }
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $app['session']->getFlashBag()->add('danger','Unable to update '.$formData['ImageNumber']  .' Image');

            }
        }


        // remove the old image data from the database.   If a new info is to be saved.

        $del_sql = "delete from image where member_id = :member_id and ImageNumber = :image_number";
        $del_stmt = $dbo->prepare ($del_sql);
        $del_stmt->bindParam(':image_number', $formData['ImageNumber'], \PDO::PARAM_INT);
        $del_stmt->bindParam(':member_id', $memberID , \PDO::PARAM_INT);

        $delStatus = $del_stmt->execute();



        $sql = "insert into image (member_id, ImageNumber, Title, Medium, Dimensions) VALUES (:member_id, :image_number, :title, :medium, :dimensions)";

        $stmt = $dbo->prepare ($sql);

        $stmt->bindParam(':title', $formData['Title'] , \PDO::PARAM_STR);
        $stmt->bindParam(':medium', $formData['Medium'] , \PDO::PARAM_STR);
        $stmt->bindParam(':dimensions', $formData['Dimensions']  , \PDO::PARAM_STR);
        $stmt->bindParam(':member_id', $memberID , \PDO::PARAM_INT);
        $stmt->bindParam(':image_number', $formData['ImageNumber'] , \PDO::PARAM_INT);

        if ($stmt->execute()) {

            $app['session']->getFlashBag()->add('info','Text for image '.$formData['ImageNumber']  .' updated successfully');

        } else {


            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            $resultData = null;
        }

        $resultData = 5;
        return $resultData;


    }


}