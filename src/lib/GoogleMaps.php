<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 1/4/18
 * Time: 11:15 PM
 */

namespace SOS;


define("MAPS_HOST", "maps.google.com");


class GoogleMaps
{

    function sos_geocode_key($sos_dbo, $key, $location_id, $addr1)
    {
        $debug = false;
        $delay = 2500;
        if (empty($key)){
            $key = $this->getAPIkey();
        }
        $base_url = "https://" . MAPS_HOST . "/maps/geo?output=xml" . "&key=" . $key;

        // Try to geocode the location.
        $geocode_pending = true;

        // try short address first, then long
        $try = 0;

        while (($geocode_pending) && ($try <= 1)) {

            usleep($delay);

            if ($try == 0) {
                $address = $addr1;
            } else {
                $address = $addr1;
            }

            $try = $try + 1;
            if ($debug)
                echo "ADDRESS: $address<br>";
            $request_url = $base_url . "&q=" . urlencode($address);

            $request_url = "https://maps.googleapis.com/maps/api/geocode/json?key=" . $key. "&address=" . urlencode($address) . "&sensor=false";
            //       do_dump ($request_url);echo"<br>";

            $jReturn = file_get_contents($request_url);

            $data = json_decode($jReturn);

//        $status = $xml->Response->Status->code;
            $status = $data->status;
            if ($debug) {
                var_dump($data);
                echo "<br>";
            }
            // ---------------------------------------
            //Check if geocode worked

            //         print"in Geocode status= $status<br>$address<br> ";


            // Successful geocode

            if (strcmp($status, "OK") == 0 && isset($data->results[0])) {

                $geocode_pending = false;
                // Format: Longitude, Latitude, Altitude
                $lat = $data->results[0]->geometry->location->lat;
                $long = $data->results[0]->geometry->location->lng;
                if ($debug) {
                    echo "<br>Lat lont $lat, $long<br>";
                }
                $sql = " UPDATE `locations` SET `lat` = :lat ,`lng` = :lng WHERE `location_id` =:location_id LIMIT 1 ;";
                $stmt = $sos_dbo->prepare($sql);
                $stmt->bindParam(':lat', $lat, \PDO::PARAM_STR);
                $stmt->bindParam(':lng', $long, \PDO::PARAM_STR);
                $stmt->bindParam(':location_id', $location_id, \PDO::PARAM_STR);

                if (  $stmt->execute()){

                } else {

                    if (\SOSModels\Globals::$sql_debug) {
                        echo "\nPDO::errorInfo():\n";
                        print_r($stmt->errorInfo());
                    }
                }


                // too many requests in too short a time?

            } else if (strcmp($status, "620") == 0) {


                // sent geocodes too fast
                $delay += 100000;

            }
        } // while geocode pending

    }


    public function getAPIkey ()
    {
        // this is the key for geocoding

        return 'AIzaSyA101IuRXIsQ0lzVObJ7uEXZTx-xCLDrtY';
        // 2019 this is the restricted url key

        //return "AIzaSyBcX6O6spnQHwhY3BHEUaf0WyE_oarj6ho";

    }
}


