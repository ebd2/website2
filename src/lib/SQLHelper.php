<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 1/4/18
 * Time: 11:15 PM
 */

namespace SOS;


class SQLHelper {


    /* from data:
    'business_name' => string 'my dev' (length=6)
  'first_name' => string 'Aram' (length=4)
  'last_name' => string 'Comjean' (length=7)
  'email' => string 'acomjean2@gmail.com' (length=19)
  'phone' => string '6173310156' (length=10)
  'street_address' => string '1716 cambridge st #11' (length=21)
  'street_address2' => null
  'city' => string 'cambridge' (length=9)
  'state' => string 'Massachusetts' (length=13)
  'zip_code' => string '02138' (length=5)
  'date' => string '2018-01-07 22:34:53' (length=19)
  'year' => string '2018' (length=4)
  'type' => string 'trolley' (length=7)

    $paramsToBind = array('business_name' =>'db_col_name_business_name',
            'street_address' =>'db_col_name_street',
            'state' =>'db_col_name_state',
            'zip_code' =>'db_col_name_statezip',
            'phone' =>'db_col_phone',
            'type' =>'db_col_type',
            'year'=> 'db_col_year',
            'city' => 'city',
            'email'=>'contact_email',
            'first_name'=>'contact_fname',
            'last_name'=>'contact_lname',
            'date'=>'date'


    */

    public static function bindSQL($dbo, $sql, $paramsToBind, $formData)
    {
        //var_dump ($sql);
        $stmt = $dbo->prepare ($sql);

        //var_dump ($formData);
        // loop thought the form data
        foreach ($formData as $key=>$value){

            // only bind elements if they're part of the params to bind array
            if (  array_key_exists($key, $paramsToBind)  ){
                $insert_string = ":".$paramsToBind[$key];
                $stmt->bindParam($insert_string, $formData[$key] , \PDO::PARAM_STR);

            }

        }

        return $stmt;


    }

    public static function findChangesInArray($keysToCheck, $originalData, $newData)
    {
        // make a list of the changed fields, we will update only those;
        $changedFields = array();

        foreach ($keysToCheck as $oneFieldName) {

            if (array_key_exists($oneFieldName, $newData) && $originalData[$oneFieldName] != $newData[$oneFieldName]) {

                $changedFields[$oneFieldName] = $newData[$oneFieldName];
                $status['messages'][] = "updating $oneFieldName to {$newData[$oneFieldName]}";
            }
        }
        return $changedFields;
    }



}