<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/2/17
 * Time: 6:44 PM
 */

namespace SOS;


class artistsGenreList {
    // constructor.


    function __construct() {

    }

    /* this array maps a 'Y/N' genre column name in the member table
        to the genre name that we want to display on an artist's profile page */

    public static $column_name_to_genre_name = array(
        'Books' => 'Books + Paper',
        'Collage' => 'Collage',
        'Drawing' => 'Drawing',
        'Fiber' => 'Fiber + Textiles',
        'Furniture' => 'Furniture',
        'Glass' => 'Glass + Mosiacs',
        'Graphic' => 'Graphic Design',
        'Installation' => 'Installation',
        'Jewelry' => 'Jewelry + Beads',
        'Mixed_Media' => 'Mixed-Media',
//     'Multimedia' => 'Multimedia',  removed for sos2015
        'Painting' => 'Painting',
        'Photography' => 'Photography',
        'Pottery' => 'Pottery',
        'Printmaking' => 'Printmaking',
        'Sculpture' => 'Sculpture',
        'Video' => 'Video',
        'Other_Media' => 'Other');


    /* given a database row from the member table,
       return a comma-space separated list of this member's art genres.
       If the Other_Media column is 'Y', append the artist's "Other" string (if any) */

    public static function getGenreText($data) {
        $mtype = '';
        foreach (self::$column_name_to_genre_name as $column => $genre) {
            if ($data[$column] == 'Y') {
                $mtype .= "$genre, ";
            }
        }
        $mtype = rtrim($mtype, ", ");  // strip trailing separator
        if (substr($mtype, -5) == "Other") {
            $other = $data['Other'];
            if ($other != '') {
                $mtype .= ": $other";
            }
        }
        return $mtype;
    }

    public function getGenreFormCheckboxes() {
        foreach (self::$column_name_to_genre_name as $column => $genre) {


        }

    }

}