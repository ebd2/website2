<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/10/15
 * Time: 8:50 PM
 */
//use Zend\Crypt\Password\Bcrypt;
//use Silex\Application;

namespace SOS;


class LoginService
{


    private $salt = '<html> header <html>';
    private $sos_dbo;
// this  a bridge class.  That allows users to login with the old system
// and updates the password in the new table

// Variables


    function __construct($dbo, \Silex\Application $app)
    {
        $this->sos_dbo = $dbo;
        //$app->register(new Silex\Provider\SessionServiceProvider());

    }



    public function generatePW($password)
    {
        $options = [
            'cost' => 10,
        ];

        $securePass = password_hash("$password", PASSWORD_BCRYPT, $options);

        //$bcrypt = new \Zend\Crypt\Password\Bcrypt();
        //$bcrypt->setCost(10);
        //$securePass = $bcrypt->create($password);

        return $securePass;
    }


    public function checkAutoLogin($uid, $hash)
    {
        $verify = false;
        $returnObj = new \stdClass();
        $returnObj->result = false;
        $returnObj->memberRow = null;




        $sql = "SELECT * from member  WHERE id = :userID AND temp_login = :tempCred";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindParam (':userID',$uid, \PDO::PARAM_STR );
        $stmt->bindParam (':tempCred',$hash, \PDO::PARAM_STR );
        $stmt->execute();

        $all_rows= $stmt->fetchAll();


        //  var_dump ($all_rows);

        if (empty($all_rows)) {
            return $returnObj;
        } else {

            $sql = "UPDATE `member` SET `year_ok` = '2019' WHERE `id` =  :userID";
            $stmt = $this->sos_dbo->prepare($sql);
            $stmt->bindParam (':userID',$uid, \PDO::PARAM_STR );
            $stmt->execute();

            $result = $all_rows[0];

            $returnObj->result = true;
            $returnObj->memberRow = $result;
            return $returnObj;
        }
    }


    public function checkLogin($email, $password, $legacyPW = null)
    {

        $verify = false;
        $returnObj = new \stdClass();
        $returnObj->result = false;
        $returnObj->memberRow = null;

        $sql = 'SELECT id, EmailAddress, epassword, password, Active, MemberType, Expanded_Web, Pending, Pending_Date FROM member WHERE EmailAddress=:email_address';

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindParam(':email_address', $email, \PDO::PARAM_STR);
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (empty($results)) {
            return $returnObj;
        }

        $result = $results[0];

        $options = [
            'cost' => 10,
        ];

        $securePass = password_hash("$password", PASSWORD_BCRYPT, $options);

        //$bcrypt = new \Zend\Crypt\Password\Bcrypt();
        //$bcrypt->setCost(10);

        // 1 check if new password exists.

        if (!empty($result['password'])) {

            $hash = $result['password'];
            $verify = password_verify($password, $hash);
            //$verify = $bcrypt->verify($password, $hash);

        } else {

            // this is legacy.  The part that removed the old passwod had been removed.
                $verify = false;
        }

        if ($verify == true) {
            $returnObj->result = true;
        }
        $returnObj->memberRow = $result;

        return $returnObj;
    }


    public function simput_encrypt($string_to_en)
    {
        $clean = array();

        $encrypted_pw = md5($this->salt . $string_to_en);

        return ($encrypted_pw);


    }

    public function generate_new_pw()

    {

        $words = array('appleDS5#DF', 'pearS#DF', 'kiwS3#$DFi', 'oran$%gge', 'gr*&^#ape', 'bl(#$?><ue', 'gre<>en', 'viol*(&^@et');

        $count = count($words);

        $new_pw = sprintf('%s$02%s', $words[mt_rand(0, $count - 1)], mt_rand(0, 999), $words[mt_rand(0, $count - 1)]);

        return ($new_pw);

    }


    public function resetPasswordRequest($username){

        $returnObj = new \stdClass();
        $returnObj->didReset = false;
        $returnObj->infoArray = array();
        $returnObj->warningArray = array();

        // Querey database for users
        $sql='SELECT id, EmailAddress, epassword, Active, MemberType, Expanded_Web, Pending FROM member WHERE EmailAddress=:email_address';

        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':email_address',$username, \PDO::PARAM_STR );
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        if (count($results) > 0) {

            $result = $results[0];
            // print_r($result);
            $local_id = $result['id'];

            $new_pw = $this->generate_new_pw();
            $hash = sha1($new_pw);
            $mysql_date = date("Y-m-d H:i:s");
//print "id $local_id - $new_pw -  $hash <br>";
            // store the hash and reset date in the database

            $sql = sprintf("update member set reset_key= :reset_key, reset_date= :date where id= :id LIMIT 1");
            $stmt = $this->sos_dbo->prepare($sql);

            $stmt->bindParam(':reset_key', $hash, \PDO::PARAM_STR);
            $stmt->bindParam(':date', $mysql_date, \PDO::PARAM_STR);
            $stmt->bindParam(':id', $local_id, \PDO::PARAM_INT);
            $stmt->execute();


            $email_string = <<<EOF
                Somerville Open Studios Password Reset
      go to the following link to reset your password.

EOF;

            $email_string .= "https://" . $_SERVER['HTTP_HOST'] . "/for_artists/update_password/{$local_id}/{$hash}";

            // '.$sos_host_base.'/membership/password_update?uid='.$local_id.'&hid='$hash2.'

            //   if there are any problems email webmaster@somervilleopenstudios.org';

            $subject = "Somerville Open Studio Account info";

            //mail


            $sendto = $username;

            // dont' send till deployed
            mail($sendto, $subject, $email_string, "From: webmaster@somervilleopenstudios.org");
            $returnObj->didReset = true;
            $returnObj->infoArray[]= "Password Reset Email Sent- check your email for a link to reset your password.";

          }  else {

            $returnObj->warningArray[]="Email Address not found";



        }
        return $returnObj;
    }

    public function checkHashValue ($hash){

        $sql = "SELECT * FROM member WHERE reset_key= :hash ;";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindParam(':hash', $hash, \PDO::PARAM_STR);
        $stmt->execute();
echo "HASH $hash <br>";
        // if we got something back
        if( $results= $stmt->fetchAll(\PDO::FETCH_ASSOC) ) {

            $result= $results[0];
            $local_id=$result['id'];
            return $local_id;

        } else {


            return null;
            $errors++;
            $error_string .= "Couldn't match data from database .. If it been more than a few days since you reset you may need to <a href= \"password_reset.php\"> reset it again </a><br> If your still having problems please email webmaster@<this>somervilleopenstudios.org";
            $output_inputform=0;
        }
    }

    public function setPassword($local_id, $password){

        //$bcrypt = new  \Zend\Crypt\Password\Bcrypt();
        //$bcrypt->setCost(10);
        $securePass = $this->generatePW($password);


        $sql = 'UPDATE member set password =:password, epassword= "", reset_key= :reset_key  WHERE id= :id LIMIT 1';
        $stmt = $this->sos_dbo->prepare ($sql);

        //set reset key to date
        //    print "$mysql_date $new_pw $local_id ";
        $mysql_date=  date("Y-m-d H:i:s");

        $stmt->bindParam(':reset_key', $mysql_date , \PDO::PARAM_STR);
        $stmt->bindParam(':password', $securePass , \PDO::PARAM_STR);
        $stmt->bindParam(':id', $local_id , \PDO::PARAM_INT);
        $stmt->execute();


        return true;
    }


}


