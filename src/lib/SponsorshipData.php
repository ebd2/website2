<?php
namespace SOS;


class SponsorshipData {
// constructor.


                function getRateData (){


                    $sponsorData = array();
                    $sponsorData['food_and_drink'] = array('name'=>"Food & Drink Listing",
                        'cost'=> "95",
                        'cost_text'=> "$95",
                        'available'=>true,
                        "description"=>array( 'Text Only','Inclusion in Restaurant, Pub, & Cafe; guide in print and on the website, sorted by location'
                        ));


                    $sponsorData['mini'] = array('name'=>"Mini",
                        'cost'=> "190",
                        'cost_text'=> "$190",
                        'available'=>true,
                        "description"=> array(
                            '(1/8 page or Mini-card Size approx.) in printed mapbook<br/>',
                            'Listing on SOS website with link to your business.<br/>',
                            '<i>Inclusion in Restaurant & Caf&eacute; listing if applicable</i>',
                        ));


                    $sponsorData['basic'] = array('name'=>"Basic",
                        'available'=>true,
                        'cost'=> "350",
                        'cost_text'=> "$350",
                        "description"=>array(
                            '4"w x 2"h (1/4 page or Bus. Card Size approximate) in printed mapbook',
                            'Icon denoting business location on artist map',
                            'Reference to map region in ad',
                            'Listing on SOS website with link to your business',
                            '<i>Inclusion in Restaurant & Caf&eacute; listing if applicable</i><br/></p>',
                        ));


                    $sponsorData['silver'] = array('name'=>"Silver",
                        'available'=>true,
                        'cost'=> "600",
                        'cost_text'=> "$600",
                        "description"=> array(
                            '4"w x 4.5"h (1/2 page approximate) in printed mapbook',
                            'Icon denoting business location on artist map',
                            'Reference to map region in ad',
                            'Listing on SOS website with link to your business',
                            '<i>Inclusion in Restaurant & Caf&eacute; listing if applicable</i><br/></p>',
                        ));


                    $sponsorData['silver_plus'] = array('name'=>"Silver+",
                        'cost'=> "750",
                        'cost_text'=> "$750",
                        'available'=>true,
                        "description"=> array(
                            'All of the above, plus..',
                            'additional .75" x .75" Logo on mapbook back cover',
                            ''
                        ));


                    $sponsorData['gold'] = array('name'=>"Gold",
                        'cost'=> "900",
                        'cost_text'=> "$900",
                        'available'=>true,
                        "description"=> array(
                            '4"w x 9"h (full page) in printed mapbook',
                            'Icon denoting business location on artist map',
                            'Reference to map page in ad',
                            'Listing on SOS website with link to your business',
                            '<i>Inclusion in Restaurant & Caf&eacute; listing if applicable</i>',
                        ));


                    $sponsorData['gold_plus'] = array('name'=>"Gold+",
                        'available'=>true,
                        'cost'=> "1050",
                        'cost_text'=> "$1050",
                        "description"=> array(
                            'All of the above, plus...',
                            'additional .75" x .75" Logo on Mapbook back cover',
                            '',
                        ));


                    $sponsorData['city_poles'] =   array('name'=>"City Poles",
                        'cost'=> "500",
                        'cost_text'=> "$500",
                        'available'=>false,
                        "description"=> array(' for 5 banners w/ logo
                For two weeks prior to SOS weekend we
                will have banners on city poles throughout Union, Davis, Ball, Magoun and Teele
                Squares. ','
                Please contact us for more
                information about this option.'
                        ));



                    $sponsorData['trolley'] = array('name'=>"Trolley",
                        'cost'=> "1000",
                        'cost_text'=> "$1000",
                        'available'=>false,
                        "description"=>array( ' for one
                side, $2000 for both sides',

                            'Your banner on one or more of three free event
                trolleys that loop throughout
                the city both days of SOS weekend.',
                            'Highly visible to visitors at the event. Promote
                your business and show your support of the arts. Please
                contact us for more information about this option.'

                        ));



        return $sponsorData;

    }

}
