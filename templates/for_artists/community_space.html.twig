{% extends 'common/basic_layout.html.twig' %}
{% set active = 'artist_calendar' %}


 {% block menu %}

     {% include "common/menu_top_static.html.twig" %}

 {% endblock menu %}


{% block jumbotron %}
    <div class="jumbotron" style="margin-bottom: 0px">
        <div class="container">
            <div class="row">

                <h2>Artists' Events</h2>

            </div>
        </div>
    </div>

{% endblock jumbotron%}

{% block content %}

<div class="col-md-12">

    <div class="col-md-12">
    
       <h1>Community Space</h1> 
       
<p>While Somerville Open Studios is based on the concept of artists showing their work where it is created, we recognize that not all artists have suitable studio space that they are comfortable opening to the public. The SOS Community Space program allows Somerville artists who are unable to exhibit in their home or work-space an opportunity to participate by exhibiting in a public space with other artists.</p>

<h3>  2019 Community Space</h3>
<p>

The  2019 SOS Community Space will be held at The Armory, 191 Highland Avenue.

The space will be open to the public from 12 noon to 6 pm on Saturday and Sunday, May 4 and 5, 2019.
Community Space will not be available for the "Friday Night Preview."
</p>

<!-- MORE INFO ABOUT COMMUNITY SPACE, UNCOMMENT WHEN READY -->
<p>The  2019 community space fee is $55. This is in addition to the $65 Event Participation fee.

<p><b>Community Space Eligibility</b></p>

<p>Please note: This space is reserved for Somerville artists who are unable to show out of their home or studio. Space is limited, so please help us reserve this space for community members who have no other place to show. Because of the size of the Somerville artist community SOS is not in a position to open our event to artists from neighboring communities.</p>

<!--ul><li><p><b>Exception for 2018 only</b>:  If you previously participated in any year back to the first SOS in 1999, the residency or Somerville studios requirement is waived for this year only.  A limited number of community space slots are available to SOS alums.</ul-->


<P>Also note that artists can only register at one location. Artists cannot be registered for both community space and a home or studio.</p>

<p><b>During the Event</b></p>

<p>The community space is an extension of Somerville Open Studios, which visitors come to in order to have a direct experience with artists and their own work. Please plan to be present and with your work for all of SOS.</p>

<p><b>Showing at the Armory</b></p>

<p>While the Armory is a popular show location, there are limitations inherent to the space. Please understand that we all have to work within these limitations.</p>

<p>Wall space is extremely limited and not guaranteed. Most of the walls of the Armory are exposed brick with no means to hang work.  If you need to hang your work you will have to provide your own free-standing apparatus that must fit into the given space per artist.  This year, we will again hold a short demonstration  on how to make a low-cost, free-standing structure for hanging art.  Please email <a href="mailto:communityspace@somervilleopenstudios.org">communityspace@somervilleopenstudios.org</a> if you would like more information.</p>

<p>There isn't an abundance of natural light at the Armory.  There is general lighting throughout the room.  If you need more direct light you may bring  your own free-standing lamps and extension cords, however understand there are also a limited number of power outlets.</p>

<p><b>Space Allocation</b></p>

<p>Spots in the community space are available on a first-come, first-served basis. In the event that the community space is full, the option to select community space during registration will no longer be available. While registration for space in the Armory is first-served,
location within the Armory is not, and will be determined separately to best accommodate everyone's needs and ensure a good mix and variety of art.</p>

<p>We will be allotting specific spaces and enforcing the space borders to keep aisles clear and to be fair to all artists. Spaces will vary in exact dimensions and proportions, but each artist will have approx. 50 sq. feet of space, marked in advance on the floor with tape. Artists will receive a floor plan with a specific space assignment after registration closes, but in plenty of time to plan their layout.</p>

<p>If you have shown in the community space in the past, you will likely have a different space as we are trying to keep the show fresh for visitors and artists. We will do what we can to accommodate requests within these constraints,  but please stay flexible. We are not able to refund registration if you don't like your space.</p>

<p><b>Sharing a Community Space Booth</b></p>

<p>Two SOS artists may share a single Community Space booth.  Both artists must 
be Somerville artists (see <b>Community Space Eligibility</b> above).   One artist should register as "Community Space", and the other should register as 
normal, and just set their address to the Armory, so that they will only be paying once for the Community Space registration.

<p><b>Tax IDs</b></p>

<p>Massachusetts state law requires that SOS collect and report tax ID numbers from 
artists participating in Community Space. You must register in advance with the state 
for the ability to pay sales tax, and must provide your sales tax ID number to SOS no 
later than two weeks before the weekend so that we may comply with the law. If you fail to provide a tax ID, you won't be able to participate in the Community Space. 


</p>  





<h3>Community Space Questions?</h3>

<p>If you have questions, comments or are able to volunteer during SOS weekend to help with coordinating, setting up or breaking down community space, please email <a href="mailto:communityspace@somervilleopenstudios.org?Subject=Community%20Space:%20">communityspace@somervilleopenstudios.org</a> </p>    </p>




    </div>

</div>

{% endblock content %}